﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RawInput_dll;

namespace Smankusors.DropThePlane
{
    public partial class VerifyControl : Form
    {
        public IntPtr[] keyboards = new IntPtr[3];
        private bool[] pReady = new bool[3];
        RawInput rawInput;
        int time = 3;
        public VerifyControl()
        {
            InitializeComponent();
            rawInput = new RawInput(Handle, true);
            rawInput.AddMessageFilter();
            rawInput.KeyPressed += RawInput_KeyPressed;
        }

        private void RawInput_KeyPressed(object sender, RawInputEventArg e)
        {
            for (int i = 0; i < 3; i++)
            {
                if (keyboards[i] == IntPtr.Zero)
                {
                    keyboards[i] = e.KeyPressEvent.DeviceHandle;
                    break;
                }
                else if (keyboards[i] == e.KeyPressEvent.DeviceHandle)
                    break;
            }
            if (e.KeyPressEvent.DeviceHandle == keyboards[0])
            {
                if (e.KeyPressEvent.VKeyName == "A")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p1_A.BackColor = Color.Blue;
                    else
                        p1_A.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "S")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p1_S.BackColor = Color.Blue;
                    else
                        p1_S.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "D")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p1_D.BackColor = Color.Blue;
                    else
                        p1_D.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "ENTER")
                {
                    p1_Ready.BackColor = Color.Blue;
                    pReady[0] = true;
                }
            }
            else if (e.KeyPressEvent.DeviceHandle == keyboards[1])
            {
                if (e.KeyPressEvent.VKeyName == "A")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p2_A.BackColor = Color.Blue;
                    else
                        p2_A.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "S")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p2_S.BackColor = Color.Blue;
                    else
                        p2_S.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "D")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p2_D.BackColor = Color.Blue;
                    else
                        p2_D.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "ENTER")
                {
                    p2_Ready.BackColor = Color.Blue;
                    pReady[1] = true;
                }
            }
            else if (e.KeyPressEvent.DeviceHandle == keyboards[2])
            {
                if (e.KeyPressEvent.VKeyName == "A")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p3_A.BackColor = Color.Blue;
                    else
                        p3_A.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "S")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p3_S.BackColor = Color.Blue;
                    else
                        p3_S.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "D")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p3_D.BackColor = Color.Blue;
                    else
                        p3_D.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "ENTER")
                {
                    p3_Ready.BackColor = Color.Blue;
                    pReady[2] = true;
                }
            }
            if (pReady[0] && pReady[1] && pReady[2])
            {
                Text = "GET READY! 3";
                countDownTimer.Enabled = true;
            }
        }

        private void VerifyControl_FormClosed(object sender, FormClosedEventArgs e)
        {
            rawInput.DestroyHandle();
        }

        private void countDownTimer_Tick(object sender, EventArgs e)
        {
            time--;
            if (time == 2) Text = "READY NOW! 2";
            else if (time == 1) Text = "SET......... 1";
            else
            {
                countDownTimer.Enabled = false;
                Close();
            }
        }
    }
}
