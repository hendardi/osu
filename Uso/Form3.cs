﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RawInput_dll;
using AxWMPLib;
using ClassLibrary1;

namespace Uso
{
    public partial class Form3 : Form
    {
        public IntPtr[] keyboard;
        RawInput rawInput;
        Form1 f;

        Timer addBeatTimer;
        Timer gerakBeatTimer;

        double skrg;
        
        //player 1
        int combo; //combo user saat bermain
        int fontSizeCombo; //ukuran font combo saat bermain
        int score; //score user
        int rectanglePrecisionX; //posisi x rectangle precision
        int rectanglePrecisionY; //posisi y rectangle precision
        int rectanglePrecisionWidth; //lebar rectangle precision
        int rectanglePrecisionHeight; //tinggi rectangle precision
        int rectanglePrecisionSize; //ukuran precision
        //player 2
        int combo2; //combo user saat bermain
        int fontSizeCombo2; //ukuran font combo saat bermain
        int score2; //score user
        int rectangle2PrecisionX; //posisi x rectangle precision
        int rectangle2PrecisionY; //posisi y rectangle precision
        int rectangle2PrecisionWidth; //lebar rectangle precision
        int rectangle2PrecisionHeight; //tinggi rectangle precision
        int rectangle2PrecisionSize; //ukuran precision

        //player 1
        Rectangle rectangleBarBackground; //rectangle background saat bermain
        Rectangle rectangleHitBox1; //rectangle hitbox pertama
        Rectangle rectangleHitBox2; //rectangle hitbox kedua
        Rectangle rectangleHitBox3; //rectangle hitbox ketiga
        Rectangle rectangleHitBox4; //rectangle hitbox keempat
        Rectangle rectangleHitLine1; //rectangle garis penentu good
        Rectangle rectangleHitLine2; //rectangle garis penentu perfect1
        Rectangle rectangleHitLine3; //rectangle garis penentu perfect2
        Rectangle rectangleKeyBar1; //rectangle keybar pertama
        Rectangle rectangleKeyBar2; //rectangle keybar kedua
        Rectangle rectangleKeyBar3; //rectangle keybar ketiga
        Rectangle rectangleKeyBar4; //rectangle keybar keempat
        Rectangle rectanglePrecision; //rectangle presisi
        //player 2
        Rectangle rectangle2BarBackground; //rectangle background saat bermain
        Rectangle rectangle2HitBox1; //rectangle hitbox pertama
        Rectangle rectangle2HitBox2; //rectangle hitbox kedua
        Rectangle rectangle2HitBox3; //rectangle hitbox ketiga
        Rectangle rectangle2HitBox4; //rectangle hitbox keempat
        Rectangle rectangle2HitLine1; //rectangle garis penentu good
        Rectangle rectangle2HitLine2; //rectangle garis penentu perfect1
        Rectangle rectangle2HitLine3; //rectangle garis penentu perfect2
        Rectangle rectangle2KeyBar1; //rectangle keybar pertama
        Rectangle rectangle2KeyBar2; //rectangle keybar kedua
        Rectangle rectangle2KeyBar3; //rectangle keybar ketiga
        Rectangle rectangle2KeyBar4; //rectangle keybar keempat
        Rectangle rectangle2Precision; //rectangle presisi

        //var pen
        Pen penBarSplitter; //garis pemisah bar saat bermain
        Pen penBar; //garis hitbox
        //var brush
        Brush brushBlack; //brush berwarna hitam
        Brush[] brushColumn; //brush pada kolom ke i
        Brush brushFontCombo; //brush font combo

        Font fontCombo; //font combo
        Font fontScore; //font score

        Font fontCombo2; //font combo
        Font fontScore2; //font score

        Image imagePrecision; // gambar presisi player 1
        Image imagePrecision2; // gambar presisi player 2
        List<beatmap> p1;
        List<beatmap> p2;

        public Form3(Form1 f,IntPtr[] k)
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            this.BackgroundImage = Image.FromFile("Resources/Images/main menu background.png");
            this.keyboard = k;
            this.f = f;
            rawInput = new RawInput(Handle, true);
            rawInput.AddMessageFilter();
            rawInput.KeyPressed += RawInput_KeyPressed;

            addBeatTimer = new Timer();
            addBeatTimer.Interval = 500;
            addBeatTimer.Tick += AddBeatTimer_Tick;
            gerakBeatTimer = new Timer();
            gerakBeatTimer.Interval = 1000 / 60;
            gerakBeatTimer.Tick += GerakBeatTimer_Tick;

            skrg = 0;

            combo = 0;
            fontSizeCombo = 40;
            score = 0;
            rectanglePrecisionX = 230;
            rectanglePrecisionY = 120;
            rectanglePrecisionWidth = 200;
            rectanglePrecisionHeight = 150;
            rectanglePrecisionSize = 0;

            combo2 = 0;
            fontSizeCombo2 = 40;
            score2 = 0;
            rectangle2PrecisionX = 230 + f.jarak;
            rectangle2PrecisionY = 120;
            rectangle2PrecisionWidth = 200;
            rectangle2PrecisionHeight = 150;
            rectangle2PrecisionSize = 0;

            //init pen
            penBarSplitter = new Pen(Color.GhostWhite, 5);
            penBar = new Pen(Color.Silver, 15);

            brushBlack = new SolidBrush(Color.Black);
            brushColumn = new Brush[4];
            brushColumn[0] = new SolidBrush(Color.GhostWhite);
            brushColumn[1] = new SolidBrush(Color.HotPink);
            brushColumn[2] = new SolidBrush(Color.GhostWhite);
            brushColumn[3] = new SolidBrush(Color.HotPink);
            brushFontCombo = new SolidBrush(Color.Aqua);

            rectangleBarBackground = new Rectangle(197, 0, 262, this.Height);
            rectangleHitBox1 = new Rectangle(200, 500, 60, 60);
            rectangleHitBox2 = new Rectangle(265, 500, 60, 60);
            rectangleHitBox3 = new Rectangle(330, 500, 60, 60);
            rectangleHitBox4 = new Rectangle(395, 500, 60, 60);
            rectangleHitLine1 = new Rectangle(200, 460, 255, 90);
            rectangleHitLine2 = new Rectangle(200, 550, 255, 10);
            rectangleHitLine3 = new Rectangle(200, 560, 255, 10);
            rectangleKeyBar1 = new Rectangle(200, 565, 60, this.Height / 4);
            rectangleKeyBar2 = new Rectangle(265, 565, 60, this.Height / 4);
            rectangleKeyBar3 = new Rectangle(330, 565, 60, this.Height / 4);
            rectangleKeyBar4 = new Rectangle(395, 565, 60, this.Height / 4);
            rectanglePrecision = new Rectangle(rectanglePrecisionX, rectanglePrecisionY, rectanglePrecisionWidth, rectanglePrecisionHeight);

            rectangle2BarBackground = new Rectangle(197 + f.jarak, 0, 262, this.Height);
            rectangle2HitBox1 = new Rectangle(200 + f.jarak, 500, 60, 60);
            rectangle2HitBox2 = new Rectangle(265 + f.jarak, 500, 60, 60);
            rectangle2HitBox3 = new Rectangle(330 + f.jarak, 500, 60, 60);
            rectangle2HitBox4 = new Rectangle(395 + f.jarak, 500, 60, 60);
            rectangle2HitLine1 = new Rectangle(200 + f.jarak, 460, 255, 90);
            rectangle2HitLine2 = new Rectangle(200 + f.jarak, 550, 255, 10);
            rectangle2HitLine3 = new Rectangle(200 + f.jarak, 560, 255, 10);
            rectangle2KeyBar1 = new Rectangle(200 + f.jarak, 565, 60, this.Height / 4);
            rectangle2KeyBar2 = new Rectangle(265 + f.jarak, 565, 60, this.Height / 4);
            rectangle2KeyBar3 = new Rectangle(330 + f.jarak, 565, 60, this.Height / 4);
            rectangle2KeyBar4 = new Rectangle(395 + f.jarak, 565, 60, this.Height / 4);
            rectangle2Precision = new Rectangle(rectangle2PrecisionX, rectangle2PrecisionY, rectangle2PrecisionWidth, rectangle2PrecisionHeight);
            
            //player 1
            fontScore = new Font("Comic Sans MS", 30);
            //player 2
            fontScore2 = new Font("Comic Sans MS", 30);

            imagePrecision = Image.FromFile("Resources/Images/In Game/Transparent.png");
            imagePrecision2 = Image.FromFile("Resources/Images/In Game/Transparent.png");

            f.axWindowsMediaPlayer1.Ctlcontrols.stop();
            f.axWindowsMediaPlayer1.Ctlcontrols.play();
            p1 = new List<beatmap>();
            p2 = new List<beatmap>();
            addBeatTimer.Start();
            gerakBeatTimer.Start();
        }

        private void RawInput_KeyPressed(object sender, RawInputEventArg e)
        {
            if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
            {
                for (int i = 0; i < 2; i++)
                {
                    if (e.KeyPressEvent.DeviceHandle == keyboard[i])
                    {
                        if (i == 0)
                        {
                            for (int j = 0; j < p1.Count; j++)
                            {
                                if (e.KeyPressEvent.VKeyName == "A")
                                {
                                    if (p1[j].listRectangleBeatMap[0] != null)
                                    {
                                        if (rectangleHitBox1.IntersectsWith(p1[j].listRectangleBeatMap[0]) && rectangleHitLine2.IntersectsWith(p1[j].listRectangleBeatMap[0]) && rectangleHitLine3.IntersectsWith(p1[j].listRectangleBeatMap[0]))
                                        {
                                            p1[j].listRectangleBeatMap[0] = Rectangle.Empty;
                                            fontSizeCombo = 70;
                                            combo++;
                                            score += combo * 300;
                                            rectanglePrecisionSize = 100;
                                            imagePrecision = Image.FromFile("Resources/Images/In Game/300.png");
                                            break;
                                        }
                                        else if (rectangleHitBox1.IntersectsWith(p1[j].listRectangleBeatMap[0]) && rectangleHitLine1.IntersectsWith(p1[j].listRectangleBeatMap[0]) && rectangleHitLine2.IntersectsWith(p1[j].listRectangleBeatMap[0]))
                                        {
                                            p1[j].listRectangleBeatMap[0] = Rectangle.Empty;
                                            fontSizeCombo = 70;
                                            combo++;
                                            score += combo * 100;
                                            rectanglePrecisionSize = 100;
                                            imagePrecision = Image.FromFile("Resources/Images/In Game/100.png");
                                            break;
                                        }
                                        else if (rectangleHitBox1.IntersectsWith(p1[j].listRectangleBeatMap[0]) && rectangleHitLine1.IntersectsWith(p1[j].listRectangleBeatMap[0]))
                                        {
                                            p1[j].listRectangleBeatMap[0] = Rectangle.Empty;
                                            fontSizeCombo = 70;
                                            combo++;
                                            score += combo * 50;
                                            rectanglePrecisionSize = 100;
                                            imagePrecision = Image.FromFile("Resources/Images/In Game/50.png");
                                            break;
                                        }
                                    }
                                }
                                else if (e.KeyPressEvent.VKeyName == "S")
                                {
                                    if (p1[j].listRectangleBeatMap[1] != null)
                                    {
                                        if (rectangleHitBox2.IntersectsWith(p1[j].listRectangleBeatMap[1]) && rectangleHitLine2.IntersectsWith(p1[j].listRectangleBeatMap[1]) && rectangleHitLine3.IntersectsWith(p1[j].listRectangleBeatMap[1]))
                                        {
                                            p1[j].listRectangleBeatMap[1] = Rectangle.Empty;
                                            fontSizeCombo = 70;
                                            combo++;
                                            score += combo * 300;
                                            rectanglePrecisionSize = 100;
                                            imagePrecision = Image.FromFile("Resources/Images/In Game/300.png");
                                            break;
                                        }
                                        else if (rectangleHitBox2.IntersectsWith(p1[j].listRectangleBeatMap[1]) && rectangleHitLine1.IntersectsWith(p1[j].listRectangleBeatMap[1]) && rectangleHitLine2.IntersectsWith(p1[j].listRectangleBeatMap[1]))
                                        {
                                            p1[j].listRectangleBeatMap[1] = Rectangle.Empty;
                                            fontSizeCombo = 70;
                                            combo++;
                                            score += combo * 100;
                                            rectanglePrecisionSize = 100;
                                            imagePrecision = Image.FromFile("Resources/Images/In Game/100.png");
                                            break;
                                        }
                                        else if (rectangleHitBox2.IntersectsWith(p1[j].listRectangleBeatMap[1]) && rectangleHitLine1.IntersectsWith(p1[j].listRectangleBeatMap[1]))
                                        {
                                            p1[j].listRectangleBeatMap[1] = Rectangle.Empty;
                                            fontSizeCombo = 70;
                                            combo++;
                                            score += combo * 50;
                                            rectanglePrecisionSize = 100;
                                            imagePrecision = Image.FromFile("Resources/Images/In Game/50.png");
                                            break;
                                        }
                                    }
                                }
                                else if (e.KeyPressEvent.VKeyName == "J")
                                {
                                    if (p1[j].listRectangleBeatMap[2] != null)
                                    {
                                        if (rectangleHitBox3.IntersectsWith(p1[j].listRectangleBeatMap[2]) && rectangleHitLine2.IntersectsWith(p1[j].listRectangleBeatMap[2]) && rectangleHitLine3.IntersectsWith(p1[j].listRectangleBeatMap[2]))
                                        {
                                            p1[j].listRectangleBeatMap[2] = Rectangle.Empty;
                                            fontSizeCombo = 70;
                                            combo++;
                                            score += combo * 300;
                                            rectanglePrecisionSize = 100;
                                            imagePrecision = Image.FromFile("Resources/Images/In Game/300.png");
                                            break;
                                        }
                                        else if (rectangleHitBox3.IntersectsWith(p1[j].listRectangleBeatMap[2]) && rectangleHitLine1.IntersectsWith(p1[j].listRectangleBeatMap[2]) && rectangleHitLine2.IntersectsWith(p1[j].listRectangleBeatMap[2]))
                                        {
                                            p1[j].listRectangleBeatMap[2] = Rectangle.Empty;
                                            fontSizeCombo = 70;
                                            combo++;
                                            score += combo * 100;
                                            rectanglePrecisionSize = 100;
                                            imagePrecision = Image.FromFile("Resources/Images/In Game/100.png");
                                            break;
                                        }
                                        else if (rectangleHitBox3.IntersectsWith(p1[j].listRectangleBeatMap[2]) && rectangleHitLine1.IntersectsWith(p1[j].listRectangleBeatMap[2]))
                                        {
                                            p1[j].listRectangleBeatMap[2] = Rectangle.Empty;
                                            fontSizeCombo = 70;
                                            combo++;
                                            score += combo * 50;
                                            rectanglePrecisionSize = 100;
                                            imagePrecision = Image.FromFile("Resources/Images/In Game/50.png");
                                            break;
                                        }
                                    }
                                }
                                else if (e.KeyPressEvent.VKeyName == "K")
                                {
                                    if (p1[j].listRectangleBeatMap[3] != null)
                                    {
                                        if (rectangleHitBox4.IntersectsWith(p1[j].listRectangleBeatMap[3]) && rectangleHitLine2.IntersectsWith(p1[j].listRectangleBeatMap[3]) && rectangleHitLine3.IntersectsWith(p1[j].listRectangleBeatMap[3]))
                                        {
                                            p1[j].listRectangleBeatMap[3] = Rectangle.Empty;
                                            fontSizeCombo = 70;
                                            combo++;
                                            score += combo * 300;
                                            rectanglePrecisionSize = 100;
                                            imagePrecision = Image.FromFile("Resources/Images/In Game/300.png");
                                            break;
                                        }
                                        else if (rectangleHitBox4.IntersectsWith(p1[j].listRectangleBeatMap[3]) && rectangleHitLine1.IntersectsWith(p1[j].listRectangleBeatMap[3]) && rectangleHitLine2.IntersectsWith(p1[j].listRectangleBeatMap[3]))
                                        {
                                            p1[j].listRectangleBeatMap[3] = Rectangle.Empty;
                                            fontSizeCombo = 70;
                                            combo++;
                                            score += combo * 100;
                                            rectanglePrecisionSize = 100;
                                            imagePrecision = Image.FromFile("Resources/Images/In Game/100.png");
                                            break;
                                        }
                                        else if (rectangleHitBox4.IntersectsWith(p1[j].listRectangleBeatMap[3]) && rectangleHitLine1.IntersectsWith(p1[j].listRectangleBeatMap[3]))
                                        {
                                            p1[j].listRectangleBeatMap[3] = Rectangle.Empty;
                                            fontSizeCombo = 70;
                                            combo++;
                                            score += combo * 50;
                                            rectanglePrecisionSize = 100;
                                            imagePrecision = Image.FromFile("Resources/Images/In Game/50.png");
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else if (i == 1) 
                        {
                            for (int j = 0; j < p2.Count; j++)
                            {
                                if (e.KeyPressEvent.VKeyName == "A")
                                {
                                    if (p2[j].listRectangleBeatMap[0] != null)
                                    {
                                        if (rectangle2HitBox1.IntersectsWith(p2[j].listRectangleBeatMap[0]) && rectangle2HitLine2.IntersectsWith(p2[j].listRectangleBeatMap[0]) && rectangle2HitLine3.IntersectsWith(p2[j].listRectangleBeatMap[0]))
                                        {
                                            p2[j].listRectangleBeatMap[0] = Rectangle.Empty;
                                            fontSizeCombo2 = 70;
                                            combo2++;
                                            score2 += combo2 * 300;
                                            rectangle2PrecisionSize = 100;
                                            imagePrecision2 = Image.FromFile("Resources/Images/In Game/300.png");
                                            break;
                                        }
                                        else if (rectangle2HitBox1.IntersectsWith(p2[j].listRectangleBeatMap[0]) && rectangle2HitLine1.IntersectsWith(p2[j].listRectangleBeatMap[0]) && rectangle2HitLine2.IntersectsWith(p2[j].listRectangleBeatMap[0]))
                                        {
                                            p2[j].listRectangleBeatMap[0] = Rectangle.Empty;
                                            fontSizeCombo2 = 70;
                                            combo2++;
                                            score2 += combo2 * 100;
                                            rectangle2PrecisionSize = 100;
                                            imagePrecision2 = Image.FromFile("Resources/Images/In Game/100.png");
                                            break;
                                        }
                                        else if (rectangle2HitBox1.IntersectsWith(p2[j].listRectangleBeatMap[0]) && rectangle2HitLine1.IntersectsWith(p2[j].listRectangleBeatMap[0]))
                                        {
                                            p2[j].listRectangleBeatMap[0] = Rectangle.Empty;
                                            fontSizeCombo2 = 70;
                                            combo2++;
                                            score2 += combo2 * 50;
                                            rectangle2PrecisionSize = 100;
                                            imagePrecision2 = Image.FromFile("Resources/Images/In Game/50.png");
                                            break;
                                        }
                                    }
                                }
                                else if (e.KeyPressEvent.VKeyName == "S")
                                {
                                    if (p2[j].listRectangleBeatMap[1] != null)
                                    {
                                        if (rectangle2HitBox2.IntersectsWith(p2[j].listRectangleBeatMap[1]) && rectangle2HitLine2.IntersectsWith(p2[j].listRectangleBeatMap[1]) && rectangle2HitLine3.IntersectsWith(p2[j].listRectangleBeatMap[1]))
                                        {
                                            p2[j].listRectangleBeatMap[1] = Rectangle.Empty;
                                            fontSizeCombo2 = 70;
                                            combo2++;
                                            score2 += combo2 * 300;
                                            rectangle2PrecisionSize = 100;
                                            imagePrecision2 = Image.FromFile("Resources/Images/In Game/300.png");
                                            break;
                                        }
                                        else if (rectangle2HitBox2.IntersectsWith(p2[j].listRectangleBeatMap[1]) && rectangle2HitLine1.IntersectsWith(p2[j].listRectangleBeatMap[1]) && rectangle2HitLine2.IntersectsWith(p2[j].listRectangleBeatMap[1]))
                                        {
                                            p2[j].listRectangleBeatMap[1] = Rectangle.Empty;
                                            fontSizeCombo2 = 70;
                                            combo2++;
                                            score2 += combo2 * 100;
                                            rectangle2PrecisionSize = 100;
                                            imagePrecision2 = Image.FromFile("Resources/Images/In Game/100.png");
                                            break;
                                        }
                                        else if (rectangle2HitBox2.IntersectsWith(p2[j].listRectangleBeatMap[1]) && rectangle2HitLine1.IntersectsWith(p2[j].listRectangleBeatMap[1]))
                                        {
                                            p2[j].listRectangleBeatMap[1] = Rectangle.Empty;
                                            fontSizeCombo2 = 70;
                                            combo2++;
                                            score2 += combo2 * 50;
                                            rectangle2PrecisionSize = 100;
                                            imagePrecision2 = Image.FromFile("Resources/Images/In Game/50.png");
                                            break;
                                        }
                                    }
                                }
                                else if (e.KeyPressEvent.VKeyName == "J")
                                {
                                    if (p2[j].listRectangleBeatMap[2] != null)
                                    {
                                        if (rectangle2HitBox3.IntersectsWith(p2[j].listRectangleBeatMap[2]) && rectangle2HitLine2.IntersectsWith(p2[j].listRectangleBeatMap[2]) && rectangle2HitLine3.IntersectsWith(p2[j].listRectangleBeatMap[2]))
                                        {
                                            p2[j].listRectangleBeatMap[2] = Rectangle.Empty;
                                            fontSizeCombo2 = 70;
                                            combo2++;
                                            score2 += combo2 * 300;
                                            rectangle2PrecisionSize = 100;
                                            imagePrecision2 = Image.FromFile("Resources/Images/In Game/300.png");
                                            break;
                                        }
                                        else if (rectangle2HitBox3.IntersectsWith(p2[j].listRectangleBeatMap[2]) && rectangle2HitLine1.IntersectsWith(p2[j].listRectangleBeatMap[2]) && rectangle2HitLine2.IntersectsWith(p2[j].listRectangleBeatMap[2]))
                                        {
                                            p2[j].listRectangleBeatMap[2] = Rectangle.Empty;
                                            fontSizeCombo2 = 70;
                                            combo2++;
                                            score2 += combo2 * 100;
                                            rectangle2PrecisionSize = 100;
                                            imagePrecision2 = Image.FromFile("Resources/Images/In Game/100.png");
                                            break;
                                        }
                                        else if (rectangle2HitBox3.IntersectsWith(p2[j].listRectangleBeatMap[2]) && rectangle2HitLine1.IntersectsWith(p2[j].listRectangleBeatMap[2]))
                                        {
                                            p2[j].listRectangleBeatMap[2] = Rectangle.Empty;
                                            fontSizeCombo2 = 70;
                                            combo2++;
                                            score2 += combo2 * 50;
                                            rectangle2PrecisionSize = 100;
                                            imagePrecision2 = Image.FromFile("Resources/Images/In Game/50.png");
                                            break;
                                        }
                                    }
                                }
                                else if (e.KeyPressEvent.VKeyName == "K")
                                {
                                    if (p2[j].listRectangleBeatMap[3] != null)
                                    {
                                        if (rectangle2HitBox4.IntersectsWith(p2[j].listRectangleBeatMap[3]) && rectangle2HitLine2.IntersectsWith(p2[j].listRectangleBeatMap[3]) && rectangle2HitLine3.IntersectsWith(p2[j].listRectangleBeatMap[3]))
                                        {
                                            p2[j].listRectangleBeatMap[3] = Rectangle.Empty;
                                            fontSizeCombo2 = 70;
                                            combo2++;
                                            score2 += combo2 * 300;
                                            rectangle2PrecisionSize = 100;
                                            imagePrecision2 = Image.FromFile("Resources/Images/In Game/300.png");
                                            break;
                                        }
                                        else if (rectangle2HitBox4.IntersectsWith(p2[j].listRectangleBeatMap[3]) && rectangle2HitLine1.IntersectsWith(p2[j].listRectangleBeatMap[3]) && rectangle2HitLine2.IntersectsWith(p2[j].listRectangleBeatMap[3]))
                                        {
                                            p2[j].listRectangleBeatMap[3] = Rectangle.Empty;
                                            fontSizeCombo2 = 70;
                                            combo2++;
                                            score2 += combo2 * 100;
                                            rectangle2PrecisionSize = 100;
                                            imagePrecision2 = Image.FromFile("Resources/Images/In Game/100.png");
                                            break;
                                        }
                                        else if (rectangle2HitBox4.IntersectsWith(p2[j].listRectangleBeatMap[3]) && rectangle2HitLine1.IntersectsWith(p2[j].listRectangleBeatMap[3]))
                                        {
                                            p2[j].listRectangleBeatMap[3] = Rectangle.Empty;
                                            fontSizeCombo2 = 70;
                                            combo2++;
                                            score2 += combo2 * 50;
                                            rectangle2PrecisionSize = 100;
                                            imagePrecision2 = Image.FromFile("Resources/Images/In Game/50.png");
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }

        private void Form3_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            fontCombo = new Font("Courier New", fontSizeCombo, FontStyle.Bold);
            fontCombo2 = new Font("Courier New", fontSizeCombo2, FontStyle.Bold);

            g.FillRectangle(brushBlack, rectangleBarBackground);
            g.DrawLine(penBarSplitter, new Point(197, 0), new Point(197, this.Height));
            g.DrawLine(penBarSplitter, new Point(262, 0), new Point(262, this.Height));
            g.DrawLine(penBarSplitter, new Point(327, 0), new Point(327, this.Height));
            g.DrawLine(penBarSplitter, new Point(392, 0), new Point(392, this.Height));
            g.DrawLine(penBarSplitter, new Point(457, 0), new Point(457, this.Height));
            g.FillRectangle(brushColumn[0], rectangleKeyBar1);
            g.FillRectangle(brushColumn[1], rectangleKeyBar2);
            g.FillRectangle(brushColumn[2], rectangleKeyBar3);
            g.FillRectangle(brushColumn[3], rectangleKeyBar4);
            g.DrawLine(new Pen(Color.White, 2), new Point(200, 500), new Point(455, 500));
            g.DrawLine(penBar, new Point(200, 560), new Point(455, 560));
            
            foreach (beatmap bx in p1)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (bx.listRectangleBeatMap[i] != null && bx.listRectangleBeatMap[i].X != 0)
                    {
                        g.FillRectangle(brushColumn[i], bx.listRectangleBeatMap[i].X, bx.listRectangleBeatMap[i].Y, bx.listRectangleBeatMap[i].Width, bx.listRectangleBeatMap[i].Height);
                    }
                }
            }

            g.DrawString(combo + "", fontCombo, brushFontCombo, new Point(322 - (combo.ToString().Length * (int)fontCombo.Size / 2), 100 - (int)fontCombo.Size / 2));
            g.DrawString(score + "", fontScore, brushBlack, new Point(this.Width / 2 + 50 - (score.ToString().Length * (int)fontScore.Size), (int)fontScore.Size / 2));
            rectanglePrecision = new Rectangle(rectanglePrecisionX + (rectanglePrecisionSize / 2), rectanglePrecisionY + (rectanglePrecisionSize / 2), rectanglePrecisionWidth - rectanglePrecisionSize, rectanglePrecisionHeight - rectanglePrecisionSize);
            g.DrawImage(imagePrecision, rectanglePrecision);

            g.FillRectangle(brushBlack, rectangle2BarBackground);
            g.DrawLine(penBarSplitter, new Point(197 + f.jarak, 0), new Point(197 + f.jarak, this.Height));
            g.DrawLine(penBarSplitter, new Point(262 + f.jarak, 0), new Point(262 + f.jarak, this.Height));
            g.DrawLine(penBarSplitter, new Point(327 + f.jarak, 0), new Point(327 + f.jarak, this.Height));
            g.DrawLine(penBarSplitter, new Point(392 + f.jarak, 0), new Point(392 + f.jarak, this.Height));
            g.DrawLine(penBarSplitter, new Point(457 + f.jarak, 0), new Point(457 + f.jarak, this.Height));
            g.FillRectangle(brushColumn[0], rectangle2KeyBar1);
            g.FillRectangle(brushColumn[1], rectangle2KeyBar2);
            g.FillRectangle(brushColumn[2], rectangle2KeyBar3);
            g.FillRectangle(brushColumn[3], rectangle2KeyBar4);
            g.DrawLine(new Pen(Color.White, 2), new Point(200 + f.jarak, 500), new Point(455 + f.jarak, 500));
            g.DrawLine(penBar, new Point(200 + f.jarak, 560), new Point(455 + f.jarak, 560));

            foreach (beatmap bx in p2)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (bx.listRectangleBeatMap[i] != null && bx.listRectangleBeatMap[i].X != 0)
                    {
                        g.FillRectangle(brushColumn[i], bx.listRectangleBeatMap[i].X, bx.listRectangleBeatMap[i].Y, bx.listRectangleBeatMap[i].Width, bx.listRectangleBeatMap[i].Height);
                    }
                }
            }

            g.DrawString(combo2 + "", fontCombo2, brushFontCombo, new Point(322 + f.jarak - (combo2.ToString().Length * (int)fontCombo2.Size / 2), 100 - (int)fontCombo2.Size / 2));
            g.DrawString(score2 + "", fontScore2, brushBlack, new Point(this.Width - 50 - (score2.ToString().Length * (int)fontScore2.Size), (int)fontScore2.Size / 2));
            rectangle2Precision = new Rectangle(rectangle2PrecisionX + (rectangle2PrecisionSize / 2), rectangle2PrecisionY + (rectangle2PrecisionSize / 2), rectangle2PrecisionWidth - rectangle2PrecisionSize, rectangle2PrecisionHeight - rectangle2PrecisionSize);
            g.DrawImage(imagePrecision2, rectangle2Precision);
        }

        private void AddBeatTimer_Tick(object sender, EventArgs e)
        {
            foreach (beatmap b in f.beats)
            {
                if (b.waktuMuncul == Math.Round(skrg, 1))
                {
                    p1.Add(b);
                }
            }

            foreach (beatmap b in f.beats2)
            {
                if (b.waktuMuncul == Math.Round(skrg, 1))
                {
                    p2.Add(b);
                }
            }

            skrg += 0.5;

            if (skrg > f.durasi)
            {
                addBeatTimer.Stop();
                gerakBeatTimer.Stop();
                if (score > score2)
                {
                    MessageBox.Show("Player 1 Win");
                }
                else if (score < score2)
                {
                    MessageBox.Show("Player 2 Win");
                }
                else
                {
                    MessageBox.Show("Draw");
                }
                this.Close();
            }
        }

        private void GerakBeatTimer_Tick(object sender, EventArgs e)
        {
            if (fontSizeCombo > 40)
            {
                fontSizeCombo -= 5;
            }

            if (rectanglePrecisionSize > 0)
            {
                rectanglePrecisionSize -= 25;
            }

            if (fontSizeCombo2 > 40)
            {
                fontSizeCombo2 -= 5;
            }

            if (rectangle2PrecisionSize > 0)
            {
                rectangle2PrecisionSize -= 25;
            }

            foreach (beatmap b in p1)
            {
                b.gerak(ref combo, ref imagePrecision, ref rectanglePrecisionSize);
            }

            foreach (beatmap b in p2)
            {
                b.gerak(ref combo2, ref imagePrecision2, ref rectangle2PrecisionSize);
            }

            this.Invalidate();
        }

        private void Form3_FormClosing(object sender, FormClosingEventArgs e)
        {
            rawInput.KeyPressed -= RawInput_KeyPressed;
            rawInput = null;
            f.axWindowsMediaPlayer1.Ctlcontrols.stop();
            f = new Form1(true);
            f.Show();
            f.inGame = false;
            f.uTimer.Start();
            f.KeyPreview = true;
            f.inSelectSong = true;
        }
    }
}
