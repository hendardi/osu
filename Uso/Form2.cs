﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RawInput_dll;

namespace Uso
{
    public partial class Form2 : Form
    {
        Form1 f;
        private bool[] pReady = new bool[2];
        RawInput rawInput;
        int time = 3;
        public IntPtr[] keyboards = new IntPtr[2];
        bool kembali;

        public Form2(Form1 f)
        {
            InitializeComponent();
            this.f = f;
            rawInput = new RawInput(Handle, true);
            rawInput.AddMessageFilter();
            rawInput.KeyPressed += RawInput_KeyPressed;
            kembali = true;
            //this.FormBorderStyle = FormBorderStyle.None;
        }

        private void RawInput_KeyPressed(object sender, RawInputEventArg e)
        {
            for (int i = 0; i < 2; i++)
            {
                if (keyboards[i] == IntPtr.Zero)
                {
                    keyboards[i] = e.KeyPressEvent.DeviceHandle;
                    break;
                }
                else if (keyboards[i] == e.KeyPressEvent.DeviceHandle)
                    break;
            }
            if (e.KeyPressEvent.DeviceHandle == keyboards[0])
            {
                if (e.KeyPressEvent.VKeyName == "A")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p1_A.BackColor = Color.Blue;
                    else
                        p1_A.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "S")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p1_S.BackColor = Color.Blue;
                    else
                        p1_S.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "J")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p1_J.BackColor = Color.Blue;
                    else
                        p1_J.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "K") {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        label2.BackColor = Color.Blue;
                    else
                        label2.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "ENTER")
                {
                    p1_Ready.BackColor = Color.Blue;
                    pReady[0] = true;
                }
            }
            else if (e.KeyPressEvent.DeviceHandle == keyboards[1])
            {
                if (e.KeyPressEvent.VKeyName == "A")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p2_A.BackColor = Color.Blue;
                    else
                        p2_A.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "S")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p2_S.BackColor = Color.Blue;
                    else
                        p2_S.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "J")
                {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        p2_J.BackColor = Color.Blue;
                    else
                        p2_J.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "K") {
                    if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN)
                        label3.BackColor = Color.Blue;
                    else
                        label3.BackColor = Color.Green;
                }
                else if (e.KeyPressEvent.VKeyName == "ENTER")
                {
                    p2_Ready.BackColor = Color.Blue;
                    pReady[1] = true;
                }
            }
            
            if (pReady[0] && pReady[1])
            {
                Text = "GET READY! 3";
                countDownTimer.Enabled = true;
            }
        }

        private void VerifyControl_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void countDownTimer_Tick(object sender, EventArgs e)
        {
            time--;
            if (time == 2) Text = "READY NOW! 2";
            else if (time == 1) Text = "SET......... 1";
            else
            {
                countDownTimer.Enabled = false;
                kembali = false;
                this.Close();
            }
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            countDownTimer.Stop();

            if (!kembali)
            {
                Form3 fff = new Form3(f, keyboards);
                fff.Show();
            }
            else
            {
                rawInput.KeyPressed -= RawInput_KeyPressed;
                rawInput = null;
                f.axWindowsMediaPlayer1.Ctlcontrols.stop();
                f = new Form1(kembali);
                f.Show();
            }
        }
    }
}

