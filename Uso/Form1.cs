﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using ClassLibrary1;
using AxWMPLib;
using WMPLib;
using System.Media;
using RawInput_dll;

namespace Uso
{
    public partial class Form1 : Form
    {
        //var random
        Random rand = new Random();

        //var class
        user user;

        //var timer
        public Timer uTimer;
        Timer addBeatTimer;
        Timer gerakBeatTimer;

        //var keys
        Keys[] key;

        //rawinput
        RawInput kbot;

        //var arrlist
        public List<user> listUser;
        public List<String> songPaths;
        public List<beatmap> beats = new List<beatmap>();
        public List<beatmap> beats2 = new List<beatmap>();
        public List<beatmap> gambar = new List<beatmap>();
        public List<highscore> listHighscore = new List<highscore>();

        //var bool
        bool mainMenu; //sekarang ada di main menu atau tidak
        bool toggleSignInBackground; //apakah menu signin sedang aktif
        bool login; //ada user yang login atau tidak
        bool signInAdmin; //apakah admin sedang login
        bool toggleMenuLogo; //apakah menu logo sedang aktif
        public bool inSelectSong; //sekarang ada di menu pilih lagu atau tidak
        public bool inGame; //sudah pilih lagu dan masuk game atau belum
        bool singleplayer; //multiplayer atau singleplayer
        bool inOption; //ada di menu option atau tidak
        bool pause; //sedang di pause atau tidak
        bool inHighscore; //sudah selesai main atau belum
        bool notPlay; //tidak perlu play welcome see ya

        //var string
        string textSinglePlayer; //tulisan singleplayer pada menu logo
        string textMultiplayer; //tulisan multiplayer pada menu logo
        string textOption; //tulisan option pada menu logo
        string highscoreDirectory; //tempat highscore

        //var integer
        int index; //index user yang login
        double skrg; //waktu saat bermain
        public int durasi; //durasi lagu
        int random; //random ukuran logo
        int xLogo; //xlogo
        int yLogo; //ylogo
        int xTempLogo; //xlogo untuk dibandingkan
        int widthLogo; //lebar logo
        int heightLogo; // tinggi logo
        int CTRFadeInOut; //kapan header footer menghilang dan mucul lagi
        int tempVolume; //volume sebelum diubah ubah
        int selectSongPage; //sekarang ada di halaman berapa pada menu select song
        int combo; //combo user saat bermain
        int fontSizeCombo; //ukuran font combo saat bermain
        int score; //score user
        int rectanglePrecisionX; //posisi x rectangle precision
        int rectanglePrecisionY; //posisi y rectangle precision
        int rectanglePrecisionWidth; //lebar rectangle precision
        int rectanglePrecisionHeight; //tinggi rectangle precision
        int rectanglePrecisionSize; //ukuran precision
        public int jarak; //jarak player 2 dan player 1

        //var Rectangle
        Rectangle rectangleCursor; //rectangle cursor
        Rectangle rectangleLogo; //rectanbgle logo
        Rectangle rectangleMenuSingle; //rectangle singleplayer pada menu logo
        Rectangle rectangleMenuMultiplayer; //rectangle multiplayer pada menu logo
        Rectangle rectangleMenuOption; //rectangle option pada menu logo
        Rectangle rectangleHeader; //rectangle header
        Rectangle rectangleFooter; //rectangle footer
        Rectangle rectangleCurrentUser; //rectangle tempat nama user yang login
        Rectangle rectangleSignInBackground; //rectangle saat rectangle current user diklik
        Rectangle rectangleBackButton; //rectangle tombol back
        Rectangle rectangleAddSong; //rectangle add song pada menu admin
        Rectangle rectangleListUser; //rectangle list user pada menu admin
        Rectangle rectangleListLagu; //rectangle list lagu pada menu admin
        Rectangle rectangleLogout; //rectangle log out pada menu admin
        Rectangle prevbtn; //rectangle tombol prev pada media player
        Rectangle playbtn; //rectangle tombol play pada media player
        Rectangle pausebtn; //rectangle tombol pause pada media player
        Rectangle stopbtn; //rectangle tombol stop pada media player
        Rectangle nextbtn; //rectangle tombol next pada media player
        Rectangle[] RectSongList; //rectangle banyak lagu yang ditampilkan pada select song
        Rectangle[] upDownBtn; //rectangle tombol prev next page pada select song
        Rectangle rectangleBarBackground; //rectangle background saat bermain
        Rectangle rectangleHitBox1; //rectangle hitbox pertama
        Rectangle rectangleHitBox2; //rectangle hitbox kedua
        Rectangle rectangleHitBox3; //rectangle hitbox ketiga
        Rectangle rectangleHitBox4; //rectangle hitbox keempat
        Rectangle rectangleHitLine1; //rectangle garis penentu good
        Rectangle rectangleHitLine2; //rectangle garis penentu perfect1
        Rectangle rectangleHitLine3; //rectangle garis penentu perfect2
        Rectangle rectangleKeyBar1; //rectangle keybar pertama
        Rectangle rectangleKeyBar2; //rectangle keybar kedua
        Rectangle rectangleKeyBar3; //rectangle keybar ketiga
        Rectangle rectangleKeyBar4; //rectangle keybar keempat
        Rectangle rectanglePrecision; //rectangle presisi
        Rectangle rectangleBackgroundHighscoreOption; //rectangle background highscore

        //var brush
        Brush brushWhite; //brush berwarna putih
        Brush brushBlack; //brush berwarna hitam
        Brush brushOpaque; //brush yang ketebalannya 0.5
        Brush brushBlackFadeInOut; //brush berwarna hitam yang ketebalannya sesuai CTRFadeInOut
        Brush brushWhiteFadeInOut; //brush berwarna putih untuk tulisan current user yang berwarna putih dan ketebalannya fix 1
        Brush[] brushColumn; //brush pada kolom ke i
        Brush brushFontCombo; //brush font combo
        
        //var font
        Font uFont; //font universal dengan format comic sans ms 20
        Font fontToggleSignIn; //font untuk tulisan sign in pada menu sign in
        Font fontSelectSongSymbol; //font pada symbol > <
        Font fontCombo; //font combo
        Font fontScore; //font score

        //var pen
        Pen penBarSplitter; //garis pemisah bar saat bermain
        Pen penBar; //garis hitbox

        //var image
        Image cursor; //cursor
        Image osuLogo; //logo osu
        Image[] MusicPlayerBtns; //tombol pada music player
        Image backButton; //gambar back
        Image imagePrecision; // gambar presisi

        //var component
        Button buttonSignIn;
        Button buttonRegister;
        Button buttonLogout;
        Button saveOption;
        TextBox tbUsername;
        TextBox tbPassword;
        FormAdmin formadmin;
        OpenFileDialog openFileDialog1; //dialog saat insert song
        TrackBar volume;
        TextBox[] tbKey;

        //var IWMPP
        IWMPPlaylist playlist; //declare playlist

        //var soundplayer
        SoundPlayer button_play_select;
        SoundPlayer button_back_select;
        SoundPlayer osu_logo_select;
        SoundPlayer button_hover;
        SoundPlayer button_solo_select;

        //var axwindowsmediaplayer
        public AxWindowsMediaPlayer axWindowsMediaPlayer1;
        AxWindowsMediaPlayer axWindowsMediaPlayer2;

        public Form1()
        {
            InitializeComponent();
            //kbot = new RawInput(Handle, true);
            //kbot.AddMessageFilter();
            //kbot.KeyPressed += Kbot_KeyPressed;
        }

        public Form1(bool play)
        {
            InitializeComponent();
            notPlay = play;
            //kbot = new RawInput(Handle, true);
            //kbot.AddMessageFilter();
            //kbot.KeyPressed += Kbot_KeyPressed;
        }

        private void Kbot_KeyPressed(object sender, RawInputEventArg e) {
            if (e.KeyPressEvent.Message == Win32.WM_KEYDOWN) {
                if (e.KeyPressEvent.VKeyName ==Keys.Oemtilde.ToString().ToUpper()) {
                    button_back_select.Play();

                    if (inGame) {
                        pause = true;
                        addBeatTimer.Stop();
                        gerakBeatTimer.Stop();
                        axWindowsMediaPlayer1.Ctlcontrols.pause();
                        Invalidate();
                    }

                    if (MessageBox.Show("Exit Game?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                        uTimer.Stop();
                        addBeatTimer.Stop();
                        gerakBeatTimer.Stop();
                        playWelcomeAndSeeya(1);
                    }
                    else {
                        button_back_select.Play();
                        if (inGame) {
                            pause = false;
                            axWindowsMediaPlayer1.Ctlcontrols.play();
                            addBeatTimer.Start();
                            gerakBeatTimer.Start();
                        }
                    }
                }

                if (inGame) {
                    if (e.KeyPressEvent.VKeyName == "P") {
                        if (!pause) {
                            pause = true;
                            gerakBeatTimer.Stop();
                            addBeatTimer.Stop();
                            axWindowsMediaPlayer1.Ctlcontrols.pause();
                        }
                        else {
                            pause = false;
                            gerakBeatTimer.Start();
                            addBeatTimer.Start();
                            axWindowsMediaPlayer1.Ctlcontrols.play();
                        }

                        Invalidate();
                    }

                    for (int i = 0; i < gambar.Count; i++) {
                        if (e.KeyPressEvent.VKeyName == key[0].ToString()) {
                            if (gambar[i].listRectangleBeatMap[0] != null) {
                                if (rectangleHitBox1.IntersectsWith(gambar[i].listRectangleBeatMap[0]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[0]) && rectangleHitLine3.IntersectsWith(gambar[i].listRectangleBeatMap[0])) {
                                    gambar[i].listRectangleBeatMap[0] = Rectangle.Empty;
                                    fontSizeCombo = 70;
                                    combo++;
                                    score += combo * 300;
                                    rectanglePrecisionSize = 100;
                                    imagePrecision = Image.FromFile("Resources/Images/In Game/300.png");
                                    break;
                                }
                                else if (rectangleHitBox1.IntersectsWith(gambar[i].listRectangleBeatMap[0]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[0]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[0])) {
                                    gambar[i].listRectangleBeatMap[0] = Rectangle.Empty;
                                    fontSizeCombo = 70;
                                    combo++;
                                    score += combo * 100;
                                    rectanglePrecisionSize = 100;
                                    imagePrecision = Image.FromFile("Resources/Images/In Game/100.png");
                                    break;
                                }
                                else if (rectangleHitBox1.IntersectsWith(gambar[i].listRectangleBeatMap[0]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[0])) {
                                    gambar[i].listRectangleBeatMap[0] = Rectangle.Empty;
                                    fontSizeCombo = 70;
                                    combo++;
                                    score += combo * 50;
                                    rectanglePrecisionSize = 100;
                                    imagePrecision = Image.FromFile("Resources/Images/In Game/50.png");
                                    break;
                                }
                            }
                        }
                        if (e.KeyPressEvent.VKeyName == key[1].ToString()) {
                            if (gambar[i].listRectangleBeatMap[1] != null) {
                                if (rectangleHitBox2.IntersectsWith(gambar[i].listRectangleBeatMap[1]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[1]) && rectangleHitLine3.IntersectsWith(gambar[i].listRectangleBeatMap[1])) {
                                    gambar[i].listRectangleBeatMap[1] = Rectangle.Empty;
                                    fontSizeCombo = 70;
                                    combo++;
                                    score += combo * 300;
                                    rectanglePrecisionSize = 100;
                                    imagePrecision = Image.FromFile("Resources/Images/In Game/300.png");
                                    break;
                                }
                                else if (rectangleHitBox2.IntersectsWith(gambar[i].listRectangleBeatMap[1]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[1]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[1])) {
                                    gambar[i].listRectangleBeatMap[1] = Rectangle.Empty;
                                    fontSizeCombo = 70;
                                    combo++;
                                    score += combo * 100;
                                    rectanglePrecisionSize = 100;
                                    imagePrecision = Image.FromFile("Resources/Images/In Game/100.png");
                                    break;
                                }
                                else if (rectangleHitBox2.IntersectsWith(gambar[i].listRectangleBeatMap[1]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[1])) {
                                    gambar[i].listRectangleBeatMap[1] = Rectangle.Empty;
                                    fontSizeCombo = 70;
                                    combo++;
                                    score += combo * 50;
                                    rectanglePrecisionSize = 100;
                                    imagePrecision = Image.FromFile("Resources/Images/In Game/50.png");
                                    break;
                                }
                            }
                        }
                        if (e.KeyPressEvent.VKeyName == key[2].ToString()) {
                            if (gambar[i].listRectangleBeatMap[2] != null) {
                                if (rectangleHitBox3.IntersectsWith(gambar[i].listRectangleBeatMap[2]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[2]) && rectangleHitLine3.IntersectsWith(gambar[i].listRectangleBeatMap[2])) {
                                    gambar[i].listRectangleBeatMap[2] = Rectangle.Empty;
                                    fontSizeCombo = 70;
                                    combo++;
                                    score += combo * 300;
                                    rectanglePrecisionSize = 100;
                                    imagePrecision = Image.FromFile("Resources/Images/In Game/300.png");
                                    break;
                                }
                                else if (rectangleHitBox3.IntersectsWith(gambar[i].listRectangleBeatMap[2]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[2]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[2])) {
                                    gambar[i].listRectangleBeatMap[2] = Rectangle.Empty;
                                    fontSizeCombo = 70;
                                    combo++;
                                    score += combo * 100;
                                    rectanglePrecisionSize = 100;
                                    imagePrecision = Image.FromFile("Resources/Images/In Game/100.png");
                                    break;
                                }
                                else if (rectangleHitBox3.IntersectsWith(gambar[i].listRectangleBeatMap[2]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[2])) {
                                    gambar[i].listRectangleBeatMap[2] = Rectangle.Empty;
                                    fontSizeCombo = 70;
                                    combo++;
                                    score += combo * 50;
                                    rectanglePrecisionSize = 100;
                                    imagePrecision = Image.FromFile("Resources/Images/In Game/50.png");
                                    break;
                                }
                            }
                        }
                        if (e.KeyPressEvent.VKeyName == key[3].ToString()) {
                            if (gambar[i].listRectangleBeatMap[3] != null) {
                                if (rectangleHitBox4.IntersectsWith(gambar[i].listRectangleBeatMap[3]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[3]) && rectangleHitLine3.IntersectsWith(gambar[i].listRectangleBeatMap[3])) {
                                    gambar[i].listRectangleBeatMap[3] = Rectangle.Empty;
                                    fontSizeCombo = 70;
                                    combo++;
                                    score += combo * 300;
                                    rectanglePrecisionSize = 100;
                                    imagePrecision = Image.FromFile("Resources/Images/In Game/300.png");
                                    break;
                                }
                                else if (rectangleHitBox4.IntersectsWith(gambar[i].listRectangleBeatMap[3]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[3]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[3])) {
                                    gambar[i].listRectangleBeatMap[3] = Rectangle.Empty;
                                    fontSizeCombo = 70;
                                    combo++;
                                    score += combo * 100;
                                    rectanglePrecisionSize = 100;
                                    imagePrecision = Image.FromFile("Resources/Images/In Game/100.png");
                                    break;
                                }
                                else if (rectangleHitBox4.IntersectsWith(gambar[i].listRectangleBeatMap[3]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[3])) {
                                    gambar[i].listRectangleBeatMap[3] = Rectangle.Empty;
                                    fontSizeCombo = 70;
                                    combo++;
                                    score += combo * 50;
                                    rectanglePrecisionSize = 100;
                                    imagePrecision = Image.FromFile("Resources/Images/In Game/50.png");
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        //Tempat Event Form
        public void AxWindowsMediaPlayer2_PlayStateChange1(object sender, _WMPOCXEvents_PlayStateChangeEvent e)
        {
            //exit game selesai seeya
            if(e.newState == 8)
            {
                this.Close();
            }
        }

        public void AxWindowsMediaPlayer2_PlayStateChange(object sender, _WMPOCXEvents_PlayStateChangeEvent e)
        {
            //play music selesai welcome.mp3
            if(e.newState == 8)
            {
                axWindowsMediaPlayer1.Ctlcontrols.play();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //load data player
            listUser = new List<user>();
            loadUser();

            //WindowsMediaPlayer
            axWindowsMediaPlayer1 = new AxWindowsMediaPlayer();
            this.Controls.Add(axWindowsMediaPlayer1);
            axWindowsMediaPlayer1.Visible = false;

            //WindowsMediaPlayer Welcome and Seeya!
            axWindowsMediaPlayer2 = new AxWindowsMediaPlayer();
            this.Controls.Add(axWindowsMediaPlayer2);
            axWindowsMediaPlayer2.Visible = false;

            //SOUND FX
            button_play_select = new SoundPlayer("Resources/SoundFX/Menu/button-play-select.wav");
            button_back_select = new SoundPlayer("Resources/SoundFX/Menu/button-back-select.wav");
            osu_logo_select = new SoundPlayer("Resources/SoundFX/Menu/osu-logo-select.wav");
            button_hover = new SoundPlayer("Resources/SoundFX/Menu/button-hover.wav");
            button_solo_select = new SoundPlayer("Resources/SoundFX/Menu/button-solo-select.wav");

            //load music player buttons
            MusicPlayerBtns = new Image[5];
            MusicPlayerBtns[0] = Image.FromFile("Resources/Images/MusicPlayer/back.png");
            MusicPlayerBtns[1] = Image.FromFile("Resources/Images/MusicPlayer/play.png");
            MusicPlayerBtns[2] = Image.FromFile("Resources/Images/MusicPlayer/pause.png");
            MusicPlayerBtns[3] = Image.FromFile("Resources/Images/MusicPlayer/stop.png");
            MusicPlayerBtns[4] = Image.FromFile("Resources/Images/MusicPlayer/next.png");

            //init openfiledialog
            openFileDialog1 = new OpenFileDialog();
            openFileDialog1.FileOk += OpenFileDialog1_FileOk;
            songPaths = new List<string>();

            //createplaylist
            loadSong();

            //load seeya and welcome
            if (!notPlay)
            {
                playWelcomeAndSeeya(0);
            }

            //init form
            this.DoubleBuffered = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            this.BackgroundImage = Image.FromFile("Resources/Images/main menu background.png");

            //init keys
            key = new Keys[4];
            key[0] = Keys.A;
            key[1] = Keys.S;
            key[2] = Keys.J;
            key[3] = Keys.K;

            //init boolean
            mainMenu = true;
            toggleSignInBackground = false;
            login = false;
            signInAdmin = false;
            toggleMenuLogo = false;
            inSelectSong = false;
            inGame = false;
            singleplayer = false;
            inOption = false;
            pause = false;
            inHighscore = false;

            //init timer
            uTimer = new Timer();
            uTimer.Interval = 1000 / 60;
            uTimer.Tick += uTimer_Tick;
            uTimer.Start();
            addBeatTimer = new Timer();
            addBeatTimer.Interval = 500;
            addBeatTimer.Tick += AddBeatTimer_Tick;
            gerakBeatTimer = new Timer();
            gerakBeatTimer.Interval = 1000 / 60;
            gerakBeatTimer.Tick += GerakBeatTimer_Tick;

            //init string
            textSinglePlayer = "Single Player";
            textMultiplayer = "Multiplayer";
            textOption = "Option";

            //init integer
            index = -1;
            skrg = 0;
            durasi = 0;
            random = 0;
            xLogo = this.Width / 4 + (this.Width / 30);
            yLogo = this.Height / 8;
            xTempLogo = this.Width / 4 + (this.Width / 30);
            widthLogo = this.Width / 2 - (this.Width / 15);
            heightLogo = this.Height - (this.Height / 4);
            CTRFadeInOut = 250;
            tempVolume = 50;
            selectSongPage = 0;
            combo = 0;
            fontSizeCombo = 40;
            score = 0;
            rectanglePrecisionX = 230;
            rectanglePrecisionY = 120;
            rectanglePrecisionWidth = 200;
            rectanglePrecisionHeight = 150;
            rectanglePrecisionSize = 0;
            jarak = 550;

            //init Rectangle
            rectangleCursor = new Rectangle(0, 0, 50, 50);
            rectangleLogo = new Rectangle(xLogo, yLogo, widthLogo, heightLogo);
            rectangleMenuSingle = new Rectangle(xLogo + widthLogo / 4, yLogo + widthLogo / 5, 350, 80);
            rectangleMenuMultiplayer = new Rectangle(xLogo + widthLogo / 4, yLogo + widthLogo / 5 + 110, 350, 80);
            rectangleMenuOption = new Rectangle(xLogo + widthLogo / 4, yLogo + widthLogo / 5 + 220, 350, 80);
            rectangleHeader = new Rectangle(0, 0, this.Width, this.Height / 10);
            rectangleFooter = new Rectangle(0, this.Height - (this.Height / 10), this.Width, this.Height / 10);
            rectangleCurrentUser = new Rectangle(0, 0, this.Width / 5, this.Height / 10);
            rectangleSignInBackground = new Rectangle(-this.Width / 2, 0, this.Width / 2, this.Height);
            rectangleBackButton = new Rectangle(25, this.Height - 70, 100, 50);
            rectangleAddSong = new Rectangle(this.Width / 2 - (this.Width / 2) / 2, this.Height / 2 - (this.Height / 2) / 2, this.Width / 2, this.Height / 15);
            rectangleListUser = new Rectangle(rectangleAddSong.X, rectangleAddSong.Y + rectangleAddSong.Height + 10, rectangleAddSong.Width, rectangleAddSong.Height);
            rectangleListLagu = new Rectangle(rectangleListUser.X, rectangleListUser.Y + rectangleListUser.Height + 10, rectangleListUser.Width, rectangleListUser.Height);
            rectangleLogout = new Rectangle(rectangleListLagu.X, rectangleListLagu.Y + rectangleListLagu.Height + 10, rectangleListLagu.Width, rectangleListLagu.Height);
            //init menu inselectsong
            int offsetY = 10; //jarak antar kotak
            int mulai = this.Height / 4; //jarak atas layar dengan kotak pertama
            int rectWidth = 800; //panjangKotak
            int rectHeight = 100; //tinggiKotak
            //Create Kotak (Usahakan edit size array saja, for ini jangan diedit)
            RectSongList = new Rectangle[3];
            for (int i = 0; i < RectSongList.Length; i++)
            {
                RectSongList[i] = new Rectangle((this.Width / 2 - rectWidth / 2), ((rectHeight + offsetY) * i) + mulai, rectWidth, rectHeight);
            }
            upDownBtn = new Rectangle[2];
            upDownBtn[0] = new Rectangle(RectSongList[0].X, RectSongList[0].Y + (rectHeight * RectSongList.Length) + (RectSongList.Length * offsetY), rectHeight, rectHeight);
            upDownBtn[1] = new Rectangle(RectSongList[0].X + RectSongList[0].Width - rectHeight, RectSongList[0].Y + (rectHeight * RectSongList.Length) + (RectSongList.Length * offsetY), rectHeight, rectHeight);
            rectangleBarBackground = new Rectangle(197, 0, 262, this.Height);
            rectangleHitBox1 = new Rectangle(200, 500, 60, 60);
            rectangleHitBox2 = new Rectangle(265, 500, 60, 60);
            rectangleHitBox3 = new Rectangle(330, 500, 60, 60);
            rectangleHitBox4 = new Rectangle(395, 500, 60, 60);
            rectangleHitLine1 = new Rectangle(200, 460, 255, 90);
            rectangleHitLine2 = new Rectangle(200, 550, 255, 10);
            rectangleHitLine3 = new Rectangle(200, 560, 255, 10);
            rectangleKeyBar1 = new Rectangle(200, 565, 60, this.Height / 4);
            rectangleKeyBar2 = new Rectangle(265, 565, 60, this.Height / 4);
            rectangleKeyBar3 = new Rectangle(330, 565, 60, this.Height / 4);
            rectangleKeyBar4 = new Rectangle(395, 565, 60, this.Height / 4);
            rectanglePrecision = new Rectangle(rectanglePrecisionX, rectanglePrecisionY, rectanglePrecisionWidth, rectanglePrecisionHeight);
            rectangleBackgroundHighscoreOption = new Rectangle(this.Width / 4, 0, this.Width / 2, this.Height);

            //init brush
            brushWhite = new SolidBrush(Color.White);
            brushBlack = new SolidBrush(Color.Black);
            brushOpaque = new SolidBrush(Color.FromArgb(128, 0, 0, 0));
            brushFontCombo = new SolidBrush(Color.Aqua);
            brushColumn = new Brush[4];
            brushColumn[0] = new SolidBrush(Color.GhostWhite);
            brushColumn[1] = new SolidBrush(Color.HotPink);
            brushColumn[2] = new SolidBrush(Color.GhostWhite);
            brushColumn[3] = new SolidBrush(Color.HotPink);

            //init font
            uFont = new Font("Comic Sans MS", 20);
            fontToggleSignIn = new Font("Comic Sans MS", 15);
            fontSelectSongSymbol = new Font("Comic Sans MS", 40);
            fontScore = new Font("Comic Sans MS", 60);

            //init pen
            penBarSplitter = new Pen(Color.GhostWhite, 5);
            penBar = new Pen(Color.Silver, 15);

            //init image
            cursor = Image.FromFile("Resources/Images/cursor.png");
            osuLogo = Image.FromFile("Resources/Images/uso logo.png");
            backButton = Image.FromFile("Resources/Images/back button.png");
            imagePrecision = Image.FromFile("Resources/Images/In Game/Transparent.png");
            
            //button signIn
            buttonSignIn = new Button();
            buttonSignIn.Text = "Sign In";
            buttonSignIn.Location = new Point(100, 480);
            buttonSignIn.Size = new Size(this.Width / 3, 30);
            buttonSignIn.Click += ButtonSignIn_Click;

            //button register
            buttonRegister = new Button();
            buttonRegister.Text = "Create an Account";
            buttonRegister.Location = new Point(100, 530);
            buttonRegister.Size = new Size(this.Width / 3, 30);
            buttonRegister.Click += ButtonRegister_Click;

            //button logout
            buttonLogout = new Button();
            buttonLogout.Text = "Log Out";
            buttonLogout.Location = new Point(230, 150);
            buttonLogout.Size = new Size(200, 40);
            buttonLogout.Font = new Font("Comic Sans MS", 15);
            buttonLogout.Click += ButtonLogout_Click;

            //button save option
            saveOption = new Button();
            saveOption.Text = "Save";
            saveOption.Location = new Point(this.Width / 2 - 100, this.Height - (this.Height / 4));
            saveOption.Size = new Size(200, 40);
            saveOption.Font = new Font("Comic Sans MS", 15);
            saveOption.Click += SaveOption_Click;

            //textbox
            tbUsername = new TextBox();
            tbUsername.Location = new Point(100, 335);
            tbUsername.Size = new Size(this.Width / 3, 30);
            tbPassword = new TextBox();
            tbPassword.PasswordChar = '•';
            tbPassword.Location = new Point(100, 405);
            tbPassword.Size = new Size(this.Width / 3, 30);
            tbKey = new TextBox[4];
            for (int i = 0; i < tbKey.Length; i++)
            {
                tbKey[i] = new TextBox();
                tbKey[i].Location = new Point(this.Width / 2, (this.Height / 4) + 70 * (i + 1) + 10);
                tbKey[i].Size = new Size(50, 100);
                tbKey[i].Text = key[i].ToString().ToUpper();
                tbKey[i].TextChanged += Form1_TextChanged;
            }

            //trackbar
            volume = new TrackBar();
            volume.Maximum = 100;
            volume.Value = 50;
            volume.Width = 250;
            volume.Height = 50;
            volume.Location = new Point(this.Width / 2 - 50, this.Height / 4);
            volume.ValueChanged += Volume_ValueChanged;
            axWindowsMediaPlayer1.settings.volume = volume.Value;
        }

        private void OpenFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            //PResourcessed OK Button
            //Songinfo
            string songname = Path.GetFileNameWithoutExtension(openFileDialog1.SafeFileName);
            string ext = Path.GetExtension(openFileDialog1.SafeFileName);
            string songpath = openFileDialog1.FileName;

            if (ext.ToLower() != ".mp3")
            {
                MessageBox.Show("File Extension Must Be MP3");
                return;
            }

            //Check Apakah Folder lagu tsb sudah ada
            if (!Directory.Exists("Resources/Songs/" + songname))
            {
                //folder tidak ada, buat folder lalu copy lagu
                //Membuat Dir(Folder Baru)
                Directory.CreateDirectory("Resources/Songs/" + songname);

                //Copy File lagu (Overwrite jika sudah ada lagu)
                File.Copy(songpath, "Resources/Songs/" + songname + "/" + songname + ".mp3", true);

                //reloadAllSong
                loadSong();
                randomBeat(songname);
                StreamWriter sw = new StreamWriter("Resources/Songs/" + songname + "/highscore.txt");
                sw.Close();
            }
            else
            {
                //folder sudah ada
                MessageBox.Show("Error, Already Exist");
            }
        }

        private void uTimer_Tick(object sender, EventArgs e)
        {
            if (mainMenu == true)
            {
                if (axWindowsMediaPlayer1.playState == WMPPlayState.wmppsPlaying)
                {
                    random = rand.Next(10) * 5;
                }
                else if (axWindowsMediaPlayer2.playState == WMPPlayState.wmppsPlaying)
                {
                    CTRFadeInOut = 250;
                }

                rectangleLogo = new Rectangle(xLogo - (random / 2), yLogo - (random / 2), widthLogo + random, heightLogo + random);
            }

            //Jika x menu pada logo belum mencapai syarat maka tambahkan
            if (toggleMenuLogo && rectangleMenuSingle.X < xTempLogo + (widthLogo / 2))
            {
                xLogo -= 10;
                rectangleMenuSingle.X += 10;
                rectangleMenuSingle = new Rectangle(rectangleMenuSingle.X, rectangleMenuSingle.Y, rectangleMenuSingle.Width, rectangleMenuSingle.Height);
                rectangleMenuMultiplayer.X += 10;
                rectangleMenuMultiplayer = new Rectangle(rectangleMenuMultiplayer.X, rectangleMenuMultiplayer.Y, rectangleMenuMultiplayer.Width, rectangleMenuMultiplayer.Height);
                rectangleMenuOption.X += 10;
                rectangleMenuOption = new Rectangle(rectangleMenuOption.X, rectangleMenuOption.Y, rectangleMenuOption.Width, rectangleMenuOption.Height);
            }
            //Jika x menu pada logo belum mencapai syarat maka kurangi
            else if (!toggleMenuLogo && rectangleMenuSingle.X > xTempLogo + widthLogo / 4)
            {
                xLogo += 10;
                rectangleMenuSingle.X -= 10;
                rectangleMenuSingle = new Rectangle(rectangleMenuSingle.X, rectangleMenuSingle.Y, rectangleMenuSingle.Width, rectangleMenuSingle.Height);
                rectangleMenuMultiplayer.X -= 10;
                rectangleMenuMultiplayer = new Rectangle(rectangleMenuMultiplayer.X, rectangleMenuMultiplayer.Y, rectangleMenuMultiplayer.Width, rectangleMenuMultiplayer.Height);
                rectangleMenuOption.X -= 10;
                rectangleMenuOption = new Rectangle(rectangleMenuOption.X, rectangleMenuOption.Y, rectangleMenuOption.Width, rectangleMenuOption.Height);
            }

            //Jika xSignInBackground belum mencapai 0 maka ditambah sampai mencapai ketengah layar
            if (toggleSignInBackground && rectangleSignInBackground.X < 0)
            {
                rectangleSignInBackground.X += 40;
                rectangleSignInBackground = new Rectangle(rectangleSignInBackground.X, 0, this.Width / 2, this.Height);
            }
            //Jika xSignInBackground belum kurang dari 0 maka dikurangi sampai menghilang dari layar
            else if (!toggleSignInBackground && rectangleSignInBackground.X > -this.Width / 2)
            {
                rectangleSignInBackground.X -= 40;
                rectangleSignInBackground = new Rectangle(rectangleSignInBackground.X, 0, this.Width / 2, this.Height);
            }

            //Kurangi sampai tingkat ketebalan(CTRFadeInOut) habis
            if (CTRFadeInOut > 0)
            {
                CTRFadeInOut -= 2;
            }
            
            Invalidate();
        }
        
        private void AddBeatTimer_Tick(object sender, EventArgs e)
        {
            foreach (beatmap b in beats)
            {
                if (b.waktuMuncul == Math.Round(skrg, 1))
                {
                    gambar.Add(b);
                }
            }

            skrg += 0.5;
            
            if (skrg > durasi - 1)
            {
                addBeatTimer.Stop();
                gerakBeatTimer.Stop();
                axWindowsMediaPlayer1.Ctlcontrols.stop();

                inGame = false;
                inHighscore = true;
                uTimer.Start();

                listHighscore.Clear();

                StreamReader sr = new StreamReader(highscoreDirectory);

                while (!sr.EndOfStream)
                {
                    string[] split = sr.ReadLine().Split('-');
                    listHighscore.Add(new highscore(int.Parse(split[0]), split[1]));
                }

                sr.Close();

                if (index == -1)
                {
                    listHighscore.Add(new highscore(score, "Unknown"));
                }
                else
                {
                    listHighscore.Add(new highscore(score, listUser[index].username));
                }

                listHighscore.Sort();

                if (listHighscore.Count > 10)
                {
                    listHighscore.RemoveAt(listHighscore.Count - 1);
                }

                StreamWriter sw = new StreamWriter(highscoreDirectory);

                for (int i = 0; i < listHighscore.Count; i++)
                {
                    sw.WriteLine(listHighscore[i].ToString());
                }

                sw.Close();
            }
        }

        private void GerakBeatTimer_Tick(object sender, EventArgs e)
        {
            if (fontSizeCombo > 40)
            {
                fontSizeCombo -= 5;
            }

            if (rectanglePrecisionSize > 0)
            {
                rectanglePrecisionSize -= 25;
            }

            foreach (beatmap b in gambar)
            {
                b.gerak(ref combo, ref imagePrecision, ref rectanglePrecisionSize);
            }

            Invalidate();
        }

        private void ButtonSignIn_Click(object sender, EventArgs e)
        {
            button_play_select.Play();

            if (tbUsername.Text == "admin" && tbPassword.Text == "admin")
            {
                button_solo_select.Play();
                toggleSignInBackground = false;
                signInAdmin = true;
                mainMenu = false;

                //Remove Login Buttons and Forms
                toggleSignInBackground = false;
                tbUsername.Text = "";
                tbPassword.Text = "";
                this.Controls.Remove(buttonSignIn);
                this.Controls.Remove(buttonRegister);
                this.Controls.Remove(tbUsername);
                this.Controls.Remove(tbPassword);
                MessageBox.Show("Welcome Admin");
            }
            else
            {
                for (int i = 0; i < listUser.Count; i++)
                {
                    if (tbUsername.Text == listUser[i].username && tbPassword.Text == listUser[i].password)
                    {
                        index = i;
                    }
                }

                if (index == -1)
                {
                    button_hover.Play();
                    MessageBox.Show("User Not Found");
                }
                else
                {
                    button_solo_select.Play();
                    MessageBox.Show("Welcome, " + listUser[index].username);
                    volume.Value = listUser[index].customOption.volume;
                    tempVolume = listUser[index].customOption.volume;
                    axWindowsMediaPlayer1.settings.volume = listUser[index].customOption.volume;
                    for (int i = 0; i < listUser[index].customOption.keybind.Length; i++)
                    {
                        key[i] = listUser[index].customOption.keybind[i];
                    }
                    removeComponent(login, inOption);
                    login = true;
                }
            }
        }

        private void ButtonRegister_Click(object sender, EventArgs e)
        {
            //Cek Akun Sudah Ada Atau Belum, Asumsi Sudah Ada
            bool register = true;

            for (int i = 0; i < listUser.Count; i++)
            {
                if (tbUsername.Text == listUser[i].username)
                {
                    register = false;
                }
            }

            //Cek Sudah Ada Atau Belum
            if (!register)
            {
                button_hover.Play();
                MessageBox.Show("Username Already Used");
            }
            else
            {
                user = new user(tbUsername.Text, tbPassword.Text);
                listUser.Add(user);
                button_solo_select.Play();
                MessageBox.Show("Successfully Registered");
                saveUser();
            }
        }

        private void ButtonLogout_Click(object sender, EventArgs e)
        {
            button_play_select.Play();
            if (MessageBox.Show("Logout?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                button_back_select.Play();
                index = -1;
                removeComponent(login, inOption);
                login = false;
            }
        }

        private void SaveOption_Click(object sender, EventArgs e)
        {
            button_back_select.Play();

            if (MessageBox.Show("Do u Want 2 Save Ur Settings?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                button_play_select.Play();
                bool cek = true;

                try
                {
                    for (int i = 0; i < key.Length; i++)
                    {

                        if ((Keys)Enum.Parse(typeof(Keys), tbKey[i].Text, true) == Keys.P)
                        {
                            cek = false;
                        }
                        else
                        {
                            key[i] = (Keys)Enum.Parse(typeof(Keys), tbKey[i].Text, true);
                        }
                    }
                }
                catch (Exception)
                {
                    cek = false;
                }

                if (index != -1)
                {
                    listUser[index].customOption.volume = volume.Value;
                    if (cek == true)
                    {
                        for (int i = 0; i < key.Length; i++)
                        {
                            listUser[index].customOption.keybind[i] = (Keys)Enum.Parse(typeof(Keys), tbKey[i].Text, true);
                        }
                    }
                    saveUser();
                }

                if (cek == true)
                {
                    for (int i = 0; i < key.Length; i++)
                    {
                        key[i] = (Keys)Enum.Parse(typeof(Keys), tbKey[i].Text, true);
                        tbKey[i].Text = key[i].ToString().ToUpper();
                    }
                }
                else
                {
                    MessageBox.Show("Failed 2 Assign Key, Invalid Key Detected");
                }
                tempVolume = volume.Value;
                axWindowsMediaPlayer1.settings.volume = volume.Value;
            }
        }

        private void Form1_TextChanged(object sender, EventArgs e)
        {
            TextBox temp = (TextBox)sender;
            if (temp.Text.Length > 1)
            {
                temp.Text = temp.Text.Substring(0, 1).ToUpper();
            }
        }

        private void Volume_ValueChanged(object sender, EventArgs e)
        {
            axWindowsMediaPlayer1.settings.volume = volume.Value;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            brushBlackFadeInOut = new SolidBrush(Color.FromArgb(CTRFadeInOut, 0, 0, 0));
            brushWhiteFadeInOut = new SolidBrush(Color.FromArgb(CTRFadeInOut, 255, 255, 255));
            fontCombo = new Font("Courier New", fontSizeCombo, FontStyle.Bold);
            
            if (!inGame && !inHighscore)
            {
                //gambar header footer
                drawHeaderFooter(g, brushOpaque, brushBlackFadeInOut);
            }

            //jika ada di main menu
            if (mainMenu)
            {
                //gambar menu logo
                g.FillRectangle(brushOpaque, rectangleMenuSingle);
                g.DrawString(textSinglePlayer, uFont, brushWhite, new Point(rectangleMenuSingle.X + rectangleMenuSingle.Width - (textSinglePlayer.Length * ((int)uFont.Size - (int)uFont.Size / 4)), rectangleMenuSingle.Y + (rectangleMenuSingle.Height / 3)));
                g.FillRectangle(brushOpaque, rectangleMenuMultiplayer);
                g.DrawString(textMultiplayer, uFont, brushWhite, new Point(rectangleMenuMultiplayer.X + rectangleMenuMultiplayer.Width - (textMultiplayer.Length * ((int)uFont.Size - (int)uFont.Size / 4)), rectangleMenuMultiplayer.Y + (rectangleMenuMultiplayer.Height / 3)));
                g.FillRectangle(brushOpaque, rectangleMenuOption);
                g.DrawString(textOption, uFont, brushWhite, new Point(rectangleMenuOption.X + rectangleMenuOption.Width - (textOption.Length * ((int)uFont.Size - (int)uFont.Size / 6)), rectangleMenuOption.Y + (rectangleMenuOption.Height / 3)));

                //gambar logo
                g.DrawImage(osuLogo, rectangleLogo);

                //gambar current user
                if (CTRFadeInOut > 128)
                {
                    drawCurrentUser(g, brushOpaque, brushWhite, uFont);
                }
                else
                {
                    drawCurrentUser(g, brushBlackFadeInOut, brushWhiteFadeInOut, uFont);
                }

                //gambar background menu signin
                g.FillRectangle(brushOpaque, rectangleSignInBackground);

                //jika player membuka menu sign in
                if (toggleSignInBackground)
                {
                    CTRFadeInOut = 250;

                    //jika signin background sudah ke tengah, tambahkan komponen
                    if (rectangleSignInBackground.X >= 0)
                    {
                        //gambar semua komponen
                        g.DrawImage(backButton, rectangleBackButton);

                        if (!login)
                        {
                            g.DrawString("SIGN IN", fontToggleSignIn, brushWhite, new Point(100, 250));
                            g.DrawString("Username", fontToggleSignIn, brushWhite, new Point(100, 300));
                            g.DrawString("Password", fontToggleSignIn, brushWhite, new Point(100, 370));
                            this.Controls.Add(tbUsername);
                            this.Controls.Add(tbPassword);
                            this.Controls.Add(buttonSignIn);
                            this.Controls.Add(buttonRegister);
                        }
                        else
                        {
                            this.Controls.Add(buttonLogout);
                        }
                    }
                }
            }
            else if(signInAdmin)
            {
                //rectangle add song
                g.FillRectangle(brushOpaque, rectangleAddSong);
                g.DrawString("Add Song", uFont, brushWhite, (rectangleAddSong.X + rectangleAddSong.Width) / 3, rectangleAddSong.Y + (rectangleAddSong.Height/2) - (int)uFont.Size);

                //rectangle list user
                g.FillRectangle(brushOpaque, rectangleListUser);
                g.DrawString("List User", uFont, brushWhite, (rectangleListUser.X + rectangleListUser.Width) / 3, rectangleListUser.Y + (rectangleListUser.Height / 2) - (int)uFont.Size);

                //rectangle list lagu
                g.FillRectangle(brushOpaque, rectangleListLagu);
                g.DrawString("List Lagu", uFont, brushWhite, (rectangleListLagu.X + rectangleListLagu.Width) / 3, rectangleListLagu.Y + (rectangleListLagu.Height / 2) - (int)uFont.Size);

                //rectangle logout
                g.FillRectangle(brushOpaque, rectangleLogout);
                g.DrawString("Logout", uFont, brushWhite, (rectangleLogout.X + rectangleLogout.Width) / 3, rectangleLogout.Y + (rectangleLogout.Height / 2) - (int)uFont.Size);
            }
            else if(inSelectSong)
            {
                for (int i = 0; i < RectSongList.Length; i++)
                {
                    g.FillRectangle(brushOpaque, RectSongList[i]);
                    try
                    {
                        //gambar tulisan
                        string songname = songPaths[i + selectSongPage * RectSongList.Length].Remove(0, songPaths[i + selectSongPage * RectSongList.Length].LastIndexOf("/") + 1);
                        g.DrawString(songname, uFont, brushWhite, RectSongList[i].X, RectSongList[i].Y);
                    }
                    catch (Exception)
                    {
                        //jika index out of bound 
                        g.DrawString("-", uFont, brushWhite, RectSongList[i].X, RectSongList[i].Y);
                    }
                }
                //gambar up down button
                g.FillEllipse(brushOpaque, upDownBtn[0]);
                g.FillEllipse(brushOpaque, upDownBtn[1]);

                g.DrawString("<", fontSelectSongSymbol, brushWhite, RectSongList[0].X + 35, RectSongList[0].Y + (100 * RectSongList.Length) + (RectSongList.Length * 10) + 10);
                g.DrawString(">", fontSelectSongSymbol, brushWhite, RectSongList[0].X + RectSongList[0].Width - 65, RectSongList[0].Y + (100 * RectSongList.Length) + (RectSongList.Length * 10) + 10);

                if (CTRFadeInOut > 128)
                {
                    g.DrawImage(backButton, rectangleBackButton);
                }
            }
            else if (inOption)
            {
                g.FillRectangle(brushOpaque, rectangleBackgroundHighscoreOption);
                g.DrawString("Volume", uFont, brushWhite, this.Width / 3, this.Height / 4);
                g.DrawString(volume.Value + "", uFont, brushWhite, this.Width - (this.Width / 3), this.Height / 4);
                g.DrawString("Key 1", uFont, brushWhite, this.Width / 3, this.Height / 4 + 70);
                g.DrawString("Key 2", uFont, brushWhite, this.Width / 3, this.Height / 4 + 140);
                g.DrawString("Key 3", uFont, brushWhite, this.Width / 3, this.Height / 4 + 210);
                g.DrawString("Key 4", uFont, brushWhite, this.Width / 3, this.Height / 4 + 280);

                if (CTRFadeInOut > 128)
                {
                    g.DrawImage(backButton, rectangleBackButton);
                }
            }
            else if (inGame && singleplayer)
            {
                g.FillRectangle(brushBlack, rectangleBarBackground);
                g.DrawLine(penBarSplitter, new Point(197, 0), new Point(197, this.Height));
                g.DrawLine(penBarSplitter, new Point(262, 0), new Point(262, this.Height));
                g.DrawLine(penBarSplitter, new Point(327, 0), new Point(327, this.Height));
                g.DrawLine(penBarSplitter, new Point(392, 0), new Point(392, this.Height));
                g.DrawLine(penBarSplitter, new Point(457, 0), new Point(457, this.Height));
                g.FillRectangle(brushColumn[0], rectangleKeyBar1);
                g.FillRectangle(brushColumn[1], rectangleKeyBar2);
                g.FillRectangle(brushColumn[2], rectangleKeyBar3);
                g.FillRectangle(brushColumn[3], rectangleKeyBar4);
                for (int i = 0; i < key.Length; i++)
                {
                    g.DrawString(key[i] + "", new Font("Comic Sans MS", 30), brushBlack, new Point(150 + ((i+1) * 60), 580));
                }
                g.DrawLine(new Pen(Color.White, 2), new Point(200, 500), new Point(455, 500));
                g.DrawLine(penBar, new Point(200, 560), new Point(455, 560));
                
                foreach (beatmap bx in gambar)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        if (bx.listRectangleBeatMap[i] != null && bx.listRectangleBeatMap[i].X != 0)
                        {
                            g.FillRectangle(brushColumn[i], bx.listRectangleBeatMap[i].X, bx.listRectangleBeatMap[i].Y, bx.listRectangleBeatMap[i].Width, bx.listRectangleBeatMap[i].Height);
                        }
                    }
                }

                g.DrawString(combo + "", fontCombo, brushFontCombo, new Point(322 - (combo.ToString().Length * (int)fontCombo.Size / 2), 100 - (int)fontCombo.Size / 2));
                g.DrawString(score + "", fontScore, brushBlack, new Point(this.Width - 50 - (score.ToString().Length * (int)fontScore.Size), (int)fontScore.Size / 2));
                rectanglePrecision = new Rectangle(rectanglePrecisionX + (rectanglePrecisionSize / 2), rectanglePrecisionY + (rectanglePrecisionSize / 2), rectanglePrecisionWidth - rectanglePrecisionSize, rectanglePrecisionHeight - rectanglePrecisionSize);
                g.DrawImage(imagePrecision, rectanglePrecision);
                g.FillEllipse(brushOpaque, new Rectangle(10, this.Height - 55, 100, 50));
                g.DrawImage(backButton, rectangleBackButton);

                if (pause)
                {
                    g.DrawString("PAUSED", fontSelectSongSymbol, brushBlack, this.Width / 2, this.Height / 2 - 100);
                }
            }
            else if (inHighscore)
            {
                g.FillRectangle(brushOpaque, rectangleBackgroundHighscoreOption);
                g.DrawString("Highscore", uFont, brushWhite, this.Width / 2 - 50, 80);
                for (int i = 0; i < listHighscore.Count; i++)
                {
                    g.DrawString(listHighscore[i].ToString(), uFont, brushWhite, this.Width / 4 + 50, 150 + (i * 50));
                }
                g.DrawString("Your Score : "+score, uFont, brushWhite, this.Width / 4 + 50, 150 + 10 * 50);
                g.FillEllipse(brushOpaque, new Rectangle(10, this.Height - 55, 100, 50));
                g.DrawImage(backButton, rectangleBackButton);
            }

            if (!inGame && !inHighscore)
            {
                //gambar media player
                drawPlayer(g, this.Width - 500, 0, 500, this.Height / 10);
            }

            //gambar cursor
            g.DrawImage(cursor, rectangleCursor);
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e) {
            rectangleCursor.X = e.X - (rectangleCursor.Width / 2);
            rectangleCursor.Y = e.Y - (rectangleCursor.Height / 2);
            //Refresh ketebalan setiap bergerak
            if (!inGame) {
                CTRFadeInOut = 250;
            }
         }

        private void Form1_Click(object sender, EventArgs e)
        {
            //Create Mouse Pointer
            MouseEventArgs me = (MouseEventArgs)e;
            Rectangle pointer = new Rectangle(me.X, me.Y, 1, 1);

            if (mainMenu)
            {
                if ((pointer.IntersectsWith(rectangleMenuSingle) || pointer.IntersectsWith(rectangleMenuMultiplayer)) && toggleMenuLogo && !toggleSignInBackground)
                {
                    button_back_select.Play();
                    mainMenu = false;
                    toggleMenuLogo = false;
                    selectSongPage = 0;
                    //updatePageCount(); //mecari Current Lagu dan set page
                    if (pointer.IntersectsWith(rectangleMenuSingle))
                    {
                        singleplayer = true;
                    }
                    else if (pointer.IntersectsWith(rectangleMenuMultiplayer))
                    {
                        singleplayer = false;
                    }
                    inSelectSong = true;
                }
                else if (pointer.IntersectsWith(rectangleMenuOption) && toggleMenuLogo && !toggleSignInBackground)
                {
                    button_back_select.Play();
                    mainMenu = false;
                    toggleMenuLogo = false;
                    inOption = true;
                    volume.Value = tempVolume;
                    this.Controls.Add(volume);
                    this.Controls.Add(saveOption);
                    for (int i = 0; i < tbKey.Length; i++)
                    {
                        tbKey[i].Text = key[i].ToString().ToUpper();
                        this.Controls.Add(tbKey[i]);
                    }
                }

                if (pointer.IntersectsWith(rectangleLogo) && !toggleSignInBackground)
                {
                    osu_logo_select.Play();
                    if (toggleMenuLogo)
                    {
                        toggleMenuLogo = false;
                    }
                    else
                    {
                        toggleMenuLogo = true;
                    }
                }
                else if (pointer.IntersectsWith(rectangleCurrentUser) && !toggleSignInBackground)
                {
                    //Play Sound FX
                    button_play_select.Play();

                    toggleSignInBackground = true;
                    rectangleSignInBackground.X = -this.Width / 2;
                }
                else if (pointer.IntersectsWith(rectangleBackButton) && toggleSignInBackground)
                {
                    //Play Sound FX
                    button_back_select.Play();

                    //Remove Semua Komponen Pada Menu SignIn
                    removeComponent(login, inOption);
                }
            }
            else if (signInAdmin)
            {
                toggleMenuLogo = false;

                if (pointer.IntersectsWith(rectangleAddSong))
                {
                    button_play_select.Play();
                    openFileDialog1.ShowDialog();
                }
                else if(pointer.IntersectsWith(rectangleListUser))
                {
                    button_play_select.Play();
                    formadmin = new FormAdmin(0);
                    formadmin.parent = this;
                    formadmin.Show();
                }
                else if(pointer.IntersectsWith(rectangleListLagu))
                {
                    button_play_select.Play();
                    formadmin = new FormAdmin(1);
                    formadmin.parent = this;
                    formadmin.Show();
                }
                else if (pointer.IntersectsWith(rectangleLogout))
                {
                    button_back_select.Play();
                    mainMenu = true;
                    signInAdmin = false;
                }
            }
            else if(inSelectSong)
            {
                for (int i = 0; i < RectSongList.Length; i++)
                {
                    if (pointer.IntersectsWith(RectSongList[i]))
                    {
                        button_back_select.Play();
                        playSong(i); //lagu Kotak ke i
                    }
                }

                if (pointer.IntersectsWith(rectangleBackButton))
                {
                    button_back_select.Play();
                    if (MessageBox.Show("Back 2 Main Menu?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        button_hover.Play();
                        mainMenu = true;
                        inSelectSong = false;
                    }
                }
                else if (pointer.IntersectsWith(upDownBtn[0]))
                {
                    button_hover.Play();
                    
                    //Up Button
                    if (selectSongPage - 1 >= 0)
                    {
                        selectSongPage--;
                        Invalidate();
                    }
                }
                else if(pointer.IntersectsWith(upDownBtn[1]))
                {
                    button_hover.Play();

                    //Down Button
                    if (selectSongPage + 1 <= songPaths.Count / 3)
                    {
                        selectSongPage++;
                        Invalidate();
                    }
                }
            }
            else if (inGame)
            {
                if (pointer.IntersectsWith(rectangleBackButton))
                {
                    button_back_select.Play();
                    pause = true;
                    addBeatTimer.Stop();
                    gerakBeatTimer.Stop();
                    axWindowsMediaPlayer1.Ctlcontrols.pause();
                    Invalidate();

                    if (MessageBox.Show("Back 2 Select Song?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        button_hover.Play();
                        inSelectSong = true;
                        inGame = false;
                        addBeatTimer.Stop();
                        gerakBeatTimer.Stop();
                        uTimer.Start();
                    }
                    else
                    {
                        button_back_select.Play();
                        addBeatTimer.Start();
                        gerakBeatTimer.Start();
                    }

                    pause = false;
                    axWindowsMediaPlayer1.Ctlcontrols.play();
                }
            }
            else if (inOption)
            {
                if (pointer.IntersectsWith(rectangleBackButton))
                {
                    button_back_select.Play();
                    if (MessageBox.Show("Back 2 Main Menu?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        button_hover.Play();
                        inOption = false;
                        mainMenu = true;
                        removeComponent(login, inOption);
                        if (index != -1)
                        {
                            axWindowsMediaPlayer1.settings.volume = listUser[index].customOption.volume;
                            volume.Value = listUser[index].customOption.volume;
                        }
                        else
                        {
                            axWindowsMediaPlayer1.settings.volume = tempVolume;
                            volume.Value = tempVolume;
                        }
                    }
                }
            }
            else if (inHighscore)
            {
                if (pointer.IntersectsWith(rectangleBackButton))
                {
                    button_back_select.Play();
                    if (MessageBox.Show("Back 2 Select Song?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                    {
                        button_hover.Play();
                        inHighscore = false;
                        inSelectSong = true;
                        axWindowsMediaPlayer1.Ctlcontrols.play();
                        uTimer.Start();
                    }
                }
            }

            if (pointer.IntersectsWith(prevbtn))
            {
                axWindowsMediaPlayer1.Ctlcontrols.previous();
                button_hover.Play();
            }
            else if (pointer.IntersectsWith(playbtn))
            {
                axWindowsMediaPlayer1.Ctlcontrols.play();
                button_hover.Play();
            }
            else if (pointer.IntersectsWith(stopbtn))
            {
                axWindowsMediaPlayer1.Ctlcontrols.stop();
                button_hover.Play();
            }
            else if (pointer.IntersectsWith(pausebtn))
            {
                axWindowsMediaPlayer1.Ctlcontrols.pause();
                button_hover.Play();
            }
            else if (pointer.IntersectsWith(nextbtn))
            {
                axWindowsMediaPlayer1.Ctlcontrols.next();
                button_hover.Play();
            }
        }

        //Tempat Procedure
        //save data user
        public void saveUser()
        {
            using (Stream s = File.Open(@"player.txt", FileMode.Create))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                bformatter.Serialize(s, listUser);
            }
        }

        //load data user
        public void loadUser()
        {
            try
            {
                using (Stream s = File.Open(@"player.txt", FileMode.Open))
                {
                    var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    List<user> temp = (List<user>)bformatter.Deserialize(s);
                    listUser = temp;
                }
            }
            catch (Exception)
            {
                saveUser();
            }
        }

        //load lagu
        public void loadSong()
        {
            //Clear Song Path List
            string[] dirs = Directory.GetDirectories("Resources/Songs/");
            songPaths.Clear();

            foreach (string dir in dirs)
            {
                //GetSongName
                string songname = dir.Remove(0, dir.LastIndexOf("/") + 1) + ".mp3";
                songPaths.Add(dir + "/" + songname);
            }

            //STOP WMP
            axWindowsMediaPlayer1.Ctlcontrols.stop();

            //ShufflePlaylist
            axWindowsMediaPlayer1.settings.setMode("shuffle", true);

            //CreatePlaylist
            playlist = axWindowsMediaPlayer1.playlistCollection.newPlaylist("Playlist");
            foreach (string path in songPaths)
            {
                IWMPMedia media = axWindowsMediaPlayer1.newMedia(path);
                playlist.appendItem(media);
            }

            axWindowsMediaPlayer1.currentPlaylist = playlist;
            axWindowsMediaPlayer1.Ctlcontrols.play();
        }

        //putar lagu yang dipilih pada inselectsong
        public void playSong(int index)
        {
            if (index + (selectSongPage * RectSongList.Length) < songPaths.Count)
            {
                int formula = index + (selectSongPage * RectSongList.Length);
                string selectedSong = songPaths[formula].Remove(0, songPaths[formula].LastIndexOf("/") + 1);

                if (MessageBox.Show("Do u Want 2 Play " + selectedSong + "?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    button_play_select.Play();
                    axWindowsMediaPlayer1.Ctlcontrols.stop();
                    imagePrecision = Image.FromFile("Resources/Images/In Game/Transparent.png");
                    skrg = 0;
                    combo = 0;
                    score = 0;
                    inGame = true;
                    inSelectSong = false;
                    uTimer.Stop();
                    addBeatTimer.Start();
                    gerakBeatTimer.Start();

                    string dir = axWindowsMediaPlayer1.currentMedia.sourceURL;
                    string cek = dir.Remove(0, dir.LastIndexOf("\\") + 1);

                    while (cek != selectedSong)
                    {
                        axWindowsMediaPlayer1.Ctlcontrols.next();
                        dir = axWindowsMediaPlayer1.currentMedia.sourceURL;
                        cek = dir.Remove(0, dir.LastIndexOf("\\") + 1);
                    }

                    highscoreDirectory = songPaths[formula].Substring(0, songPaths[formula].LastIndexOf("/") + 1) + "highscore.txt";
                    readBeat(songPaths[formula].Substring(0, songPaths[formula].LastIndexOf("/") + 1));
                    axWindowsMediaPlayer1.Ctlcontrols.play();

                    if (!singleplayer)
                    {
                        uTimer.Stop();
                        addBeatTimer.Stop();
                        gerakBeatTimer.Stop();
                        //kbot.KeyPressed -= Kbot_KeyPressed;
                        //kbot = null;
                        Form2 ff = new Form2(this);
                        this.Hide();
                        ff.Show();
                    }
                }
            }
        }

        //baca beat lagu
        public void readBeat(string directory)
        {
            //Read Beat
            gambar.Clear();
            beats.Clear();
            beats2.Clear();

            StreamReader sr = new StreamReader(directory + "beatmap.txt");
            durasi = int.Parse(sr.ReadLine());

            while (!sr.EndOfStream)
            {
                string temp = sr.ReadLine();
                string[] splice = temp.Split('-');
                beatmap b = new beatmap(Convert.ToInt32(splice[0]) * 200, Convert.ToInt32(splice[1]) * 265, Convert.ToInt32(splice[2]) * 330, Convert.ToInt32(splice[3]) * 395, Convert.ToDouble(splice[4]));
                beats.Add(b);

                int[] value = new int[4];

                for (int i = 0; i < value.Length; i++)
                {
                    if (Convert.ToInt32(splice[i]) == 0)
                    {
                        value[i] = 0;
                    }
                    else
                    {
                        value[i] = jarak;
                    }
                }
                b = new beatmap(Convert.ToInt32(splice[0]) * 200 + value[0], Convert.ToInt32(splice[1]) * 265 + value[1], Convert.ToInt32(splice[2]) * 330 + value[2], Convert.ToInt32(splice[3]) * 395 + value[3], Convert.ToDouble(splice[4]));
                beats2.Add(b);
            }

            sr.Close();
        }

        //membuat beat secara random
        public void randomBeat(string songname)
        {
            //get song info then randomize beats.
            while (axWindowsMediaPlayer1.currentMedia.name != songname)
            {
                axWindowsMediaPlayer1.Ctlcontrols.next();
            }

            //wait UserInput While Loading Song!
            MessageBox.Show("Insert Success");

            //randombeat
            Random rand = new Random();
            List<string> newBeat = new List<string>();

            //DAPAT SONG
            int duration = (int)axWindowsMediaPlayer1.currentMedia.duration;

            //addDurationToFile
            newBeat.Add(duration + "");

            int[] kotak = new int[4];
            int posisi;

            for (int i = 1; i <= duration - 5; i++)
            {
                if (rand.Next(10) > 2)
                {
                    for (int j = 0; j < kotak.Length; j++)
                    {
                        kotak[j] = 0;
                    }

                    if (rand.Next(10) > 3)
                    {
                        posisi = rand.Next(4);
                        kotak[posisi] = 1;
                    }
                    else
                    {
                        for (int k = 0; k < 2; k++)
                        {
                            do
                            {
                                posisi = rand.Next(4);

                            } while (kotak[posisi] == 1);

                            kotak[posisi] = 1;
                        }
                    }
                    
                    //add randomed beat to file
                    newBeat.Add(kotak[0] + "-" + kotak[1] + "-" + kotak[2] + "-" + kotak[3] + "-" + i);
                }
            }

            //save beat into the song folder.
            saveBeat("Resources/Songs/" + songname + "/beatmap.txt", newBeat);
        }

        public void saveBeat(string path, List<string> beat)
        {
            //save beat yang telah dibuat ke string path.
            StreamWriter sw = new StreamWriter(path);

            foreach (string line in beat)
            {
                sw.WriteLine(line);
            }

            sw.Close();

        }

        //gambar media player
        public void drawPlayer(Graphics g, int x, int y, int width, int height)
        {
            //OFFSET BUTTONS
            int offset = 15; //OFFSET JARAK ANTAR BUTTONS!
            int offsetButtons = 10; //OFFSET DARI KIRI PLAYER!
            int offsetBawah = 5; //OFFSET DARI BAWAH PLAYER!

            //SIZE BUTTONS AND FONT
            int btnscale = 20; // BESAR BTN -> 1 : btnScale
            int fontsize = 15; // Size Font

            //OFFSET FONT
            int offsetFont = 10; //OFFSET DARI KIRI PLAYER
            int offsetFontAtas = 0; //OFFSET DARI ATAS PLAYER

            /*MUSIC PLAYER*/
            //Player Background
            Rectangle bg = new Rectangle(x, y, width, height);
            Brush bgBrush;
            Brush fontBrush;
            Font f = new Font("Comic Sans MS", fontsize);

            //BUTTON COLLIDER (INTERSECT CHECK)
            prevbtn = new Rectangle(bg.X + offsetButtons + offset, bg.Y + bg.Height - bg.Width / btnscale - offsetBawah, bg.Width / btnscale, bg.Width / btnscale);
            playbtn = new Rectangle(bg.X + offsetButtons + (bg.Width / btnscale + offset) * 1 + offset, bg.Y + bg.Height - bg.Width / btnscale - offsetBawah, bg.Width / btnscale, bg.Width / btnscale);
            pausebtn = new Rectangle(bg.X + offsetButtons + (bg.Width / btnscale + offset) * 2 + offset, bg.Y + bg.Height - bg.Width / btnscale - offsetBawah, bg.Width / btnscale, bg.Width / btnscale);
            stopbtn = new Rectangle(bg.X + offsetButtons + (bg.Width / btnscale + offset) * 3 + offset, bg.Y + bg.Height - bg.Width / btnscale - offsetBawah, bg.Width / btnscale, bg.Width / btnscale);
            nextbtn = new Rectangle(bg.X + offsetButtons + (bg.Width / btnscale + offset) * 4 + offset, bg.Y + bg.Height - bg.Width / btnscale - offsetBawah, bg.Width / btnscale, bg.Width / btnscale);


            if (CTRFadeInOut > 128)
            {
                bgBrush = new SolidBrush(Color.FromArgb(128, Color.Black));
                fontBrush = new SolidBrush(Color.FromArgb(255, Color.White));
            }
            else
            {
                bgBrush = new SolidBrush(Color.FromArgb(CTRFadeInOut, Color.Black));
                fontBrush = new SolidBrush(Color.FromArgb(CTRFadeInOut, Color.White));
            }

            //DRAW PLAYER
            g.FillRectangle(bgBrush, bg);

            //DRAW BUTTONS
            g.DrawImage(changeImageOpacity(MusicPlayerBtns[0], CTRFadeInOut), bg.X + offsetButtons + offset, bg.Y + bg.Height - bg.Width / btnscale - offsetBawah, bg.Width / btnscale, bg.Width / btnscale); //PREV
            g.DrawImage(changeImageOpacity(MusicPlayerBtns[1], CTRFadeInOut), bg.X + offsetButtons + (bg.Width / btnscale + offset) * 1 + offset, bg.Y + bg.Height - bg.Width / btnscale - offsetBawah, bg.Width / btnscale, bg.Width / btnscale); //Play
            g.DrawImage(changeImageOpacity(MusicPlayerBtns[2], CTRFadeInOut), bg.X + offsetButtons + (bg.Width / btnscale + offset) * 2 + offset, bg.Y + bg.Height - bg.Width / btnscale - offsetBawah, bg.Width / btnscale, bg.Width / btnscale); //Pause
            g.DrawImage(changeImageOpacity(MusicPlayerBtns[3], CTRFadeInOut), bg.X + offsetButtons + (bg.Width / btnscale + offset) * 3 + offset, bg.Y + bg.Height - bg.Width / btnscale - offsetBawah, bg.Width / btnscale, bg.Width / btnscale); //Stop
            g.DrawImage(changeImageOpacity(MusicPlayerBtns[4], CTRFadeInOut), bg.X + offsetButtons + (bg.Width / btnscale + offset) * 4 + offset, bg.Y + bg.Height - bg.Width / btnscale - offsetBawah, bg.Width / btnscale, bg.Width / btnscale); //Next

            try
            {
                g.DrawString(axWindowsMediaPlayer1.currentMedia.name, f, fontBrush, bg.X + offsetFont, offsetFontAtas);
            }
            catch (Exception)
            {
                g.DrawString("-", f, fontBrush, bg.X + offsetFont, offsetFontAtas);
            }

            /*DEBUG DRAW COLLIDER*/
            //Pen debbuger = new Pen(Color.Red, 1);
            //g.DrawRectangle(debbuger, prevbtn);
            //g.DrawRectangle(debbuger, playbtn);
            //g.DrawRectangle(debbuger, pausebtn);
            //g.DrawRectangle(debbuger, stopbtn);
            //g.DrawRectangle(debbuger, nextbtn);
        }

        //ganti ketebalan media player
        public Bitmap changeImageOpacity(Image img, float opacityvalue)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height); // Determining Width and Height of Source Image
            Graphics graphics = Graphics.FromImage(bmp);
            ColorMatrix colormatrix = new ColorMatrix();
            colormatrix.Matrix33 = opacityvalue;
            ImageAttributes imgAttribute = new ImageAttributes();
            imgAttribute.SetColorMatrix(colormatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            graphics.DrawImage(img, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imgAttribute);
            graphics.Dispose();   // Releasing all resource used by graphics 
            return bmp;
        }

        //SFX welcome dan see you
        public void playWelcomeAndSeeya(int i)
        {
            //i == 0 WELCOME, i == 1, SEEYA
            if (i == 0)
            {
                IWMPMedia media = axWindowsMediaPlayer2.newMedia("Resources/SoundFX/welcome.mp3");
                axWindowsMediaPlayer1.Ctlcontrols.stop();
                axWindowsMediaPlayer2.currentMedia = media;
                axWindowsMediaPlayer2.Ctlcontrols.play();
                axWindowsMediaPlayer2.PlayStateChange += AxWindowsMediaPlayer2_PlayStateChange;
            }
            else if (i == 1)
            {
                IWMPMedia media = axWindowsMediaPlayer2.newMedia("Resources/SoundFX/seeya.mp3");
                axWindowsMediaPlayer1.Ctlcontrols.stop();
                axWindowsMediaPlayer2.currentMedia = media;
                axWindowsMediaPlayer2.Ctlcontrols.play();
                axWindowsMediaPlayer2.PlayStateChange += AxWindowsMediaPlayer2_PlayStateChange1;
            }
        }

        //mencari lagu lalu update select song page
        public void updatePageCount()
        {
            string lagu = axWindowsMediaPlayer1.currentMedia.name + ".mp3";

            for (int i = 0; i < songPaths.Count; i++)
            {
                string songname = songPaths[i].Remove(0, songPaths[i].LastIndexOf("/") + 1);

                if (songname == lagu)
                {
                    selectSongPage = i / RectSongList.Length;
                    return;
                }
            }
        }

        //gambar header footer
        public void drawHeaderFooter(Graphics g, Brush brushOpaque, Brush brushBlackFadeInOut)
        {
            //Jika ketebalan masih lebih dari 0.5
            if (CTRFadeInOut > 128)
            {
                g.FillRectangle(brushOpaque, rectangleHeader);
                g.FillRectangle(brushOpaque, rectangleFooter);
            }
            //Jika ketebalan sudah kurang dari 0.5
            else
            {
                g.FillRectangle(brushBlackFadeInOut, rectangleHeader);
                g.FillRectangle(brushBlackFadeInOut, rectangleFooter);
            }
        }

        //gambar current user
        public void drawCurrentUser(Graphics g, Brush brushBackground, Brush brushFont, Font uFont)
        {
            //gambar rectangle current user
            g.FillRectangle(brushBackground, rectangleCurrentUser);

            if (index == -1)
            {
                g.DrawString("Click 2 Sign In", uFont, brushFont, new Point(15, 15));
            }
            else
            {
                string concat = listUser[index].username;

                if (listUser[index].username.Length > 8)
                {
                    concat = listUser[index].username.Substring(0, 8) + "...";
                }

                g.DrawString("Hello, " + concat, uFont, brushFont, new Point(15, 15));
            }
        }

        //hapus komponen pada toggle menu signin
        public void removeComponent(bool login, bool inOption)
        {
            toggleSignInBackground = false;
            if (!login)
            {
                tbUsername.Text = "";
                tbPassword.Text = "";
                this.Controls.Remove(buttonSignIn);
                this.Controls.Remove(buttonRegister);
                this.Controls.Remove(tbUsername);
                this.Controls.Remove(tbPassword);
            }
            else
            {
                this.Controls.Remove(buttonLogout);
            }

            if (!inOption)
            {
                this.Controls.Remove(volume);
                this.Controls.Remove(saveOption);
                for (int i = 0; i < tbKey.Length; i++)
                {
                    this.Controls.Remove(tbKey[i]);
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            uTimer.Stop();
            Application.Exit();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (inGame == true && singleplayer)
            {
                if (e.KeyCode == Keys.P)
                {
                    if (!pause)
                    {
                        pause = true;
                        gerakBeatTimer.Stop();
                        addBeatTimer.Stop();
                        axWindowsMediaPlayer1.Ctlcontrols.pause();
                    }
                    else
                    {
                        pause = false;
                        gerakBeatTimer.Start();
                        addBeatTimer.Start();
                        axWindowsMediaPlayer1.Ctlcontrols.play();
                    }

                    Invalidate();
                }

                for (int i = 0; i < gambar.Count; i++)
                {
                    if (e.KeyCode == key[0])
                    {
                        if (gambar[i].listRectangleBeatMap[0] != null)
                        {
                            if (rectangleHitBox1.IntersectsWith(gambar[i].listRectangleBeatMap[0]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[0]) && rectangleHitLine3.IntersectsWith(gambar[i].listRectangleBeatMap[0]))
                            {
                                gambar[i].listRectangleBeatMap[0] = Rectangle.Empty;
                                fontSizeCombo = 70;
                                combo++;
                                score += combo * 300;
                                rectanglePrecisionSize = 100;
                                imagePrecision = Image.FromFile("Resources/Images/In Game/300.png");
                                break;
                            }
                            else if (rectangleHitBox1.IntersectsWith(gambar[i].listRectangleBeatMap[0]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[0]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[0]))
                            {
                                gambar[i].listRectangleBeatMap[0] = Rectangle.Empty;
                                fontSizeCombo = 70;
                                combo++;
                                score += combo * 100;
                                rectanglePrecisionSize = 100;
                                imagePrecision = Image.FromFile("Resources/Images/In Game/100.png");
                                break;
                            }
                            else if (rectangleHitBox1.IntersectsWith(gambar[i].listRectangleBeatMap[0]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[0]))
                            {
                                gambar[i].listRectangleBeatMap[0] = Rectangle.Empty;
                                fontSizeCombo = 70;
                                combo++;
                                score += combo * 50;
                                rectanglePrecisionSize = 100;
                                imagePrecision = Image.FromFile("Resources/Images/In Game/50.png");
                                break;
                            }
                        }
                    }
                    if (e.KeyCode == key[1])
                    {
                        if (gambar[i].listRectangleBeatMap[1] != null)
                        {
                            if (rectangleHitBox2.IntersectsWith(gambar[i].listRectangleBeatMap[1]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[1]) && rectangleHitLine3.IntersectsWith(gambar[i].listRectangleBeatMap[1]))
                            {
                                gambar[i].listRectangleBeatMap[1] = Rectangle.Empty;
                                fontSizeCombo = 70;
                                combo++;
                                score += combo * 300;
                                rectanglePrecisionSize = 100;
                                imagePrecision = Image.FromFile("Resources/Images/In Game/300.png");
                                break;
                            }
                            else if (rectangleHitBox2.IntersectsWith(gambar[i].listRectangleBeatMap[1]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[1]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[1]))
                            {
                                gambar[i].listRectangleBeatMap[1] = Rectangle.Empty;
                                fontSizeCombo = 70;
                                combo++;
                                score += combo * 100;
                                rectanglePrecisionSize = 100;
                                imagePrecision = Image.FromFile("Resources/Images/In Game/100.png");
                                break;
                            }
                            else if (rectangleHitBox2.IntersectsWith(gambar[i].listRectangleBeatMap[1]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[1]))
                            {
                                gambar[i].listRectangleBeatMap[1] = Rectangle.Empty;
                                fontSizeCombo = 70;
                                combo++;
                                score += combo * 50;
                                rectanglePrecisionSize = 100;
                                imagePrecision = Image.FromFile("Resources/Images/In Game/50.png");
                                break;
                            }
                        }
                    }
                    if (e.KeyCode == key[2])
                    {
                        if (gambar[i].listRectangleBeatMap[2] != null)
                        {
                            if (rectangleHitBox3.IntersectsWith(gambar[i].listRectangleBeatMap[2]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[2]) && rectangleHitLine3.IntersectsWith(gambar[i].listRectangleBeatMap[2]))
                            {
                                gambar[i].listRectangleBeatMap[2] = Rectangle.Empty;
                                fontSizeCombo = 70;
                                combo++;
                                score += combo * 300;
                                rectanglePrecisionSize = 100;
                                imagePrecision = Image.FromFile("Resources/Images/In Game/300.png");
                                break;
                            }
                            else if (rectangleHitBox3.IntersectsWith(gambar[i].listRectangleBeatMap[2]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[2]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[2]))
                            {
                                gambar[i].listRectangleBeatMap[2] = Rectangle.Empty;
                                fontSizeCombo = 70;
                                combo++;
                                score += combo * 100;
                                rectanglePrecisionSize = 100;
                                imagePrecision = Image.FromFile("Resources/Images/In Game/100.png");
                                break;
                            }
                            else if (rectangleHitBox3.IntersectsWith(gambar[i].listRectangleBeatMap[2]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[2]))
                            {
                                gambar[i].listRectangleBeatMap[2] = Rectangle.Empty;
                                fontSizeCombo = 70;
                                combo++;
                                score += combo * 50;
                                rectanglePrecisionSize = 100;
                                imagePrecision = Image.FromFile("Resources/Images/In Game/50.png");
                                break;
                            }
                        }
                    }
                    if (e.KeyCode == key[3])
                    {
                        if (gambar[i].listRectangleBeatMap[3] != null)
                        {
                            if (rectangleHitBox4.IntersectsWith(gambar[i].listRectangleBeatMap[3]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[3]) && rectangleHitLine3.IntersectsWith(gambar[i].listRectangleBeatMap[3]))
                            {
                                gambar[i].listRectangleBeatMap[3] = Rectangle.Empty;
                                fontSizeCombo = 70;
                                combo++;
                                score += combo * 300;
                                rectanglePrecisionSize = 100;
                                imagePrecision = Image.FromFile("Resources/Images/In Game/300.png");
                                break;
                            }
                            else if (rectangleHitBox4.IntersectsWith(gambar[i].listRectangleBeatMap[3]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[3]) && rectangleHitLine2.IntersectsWith(gambar[i].listRectangleBeatMap[3]))
                            {
                                gambar[i].listRectangleBeatMap[3] = Rectangle.Empty;
                                fontSizeCombo = 70;
                                combo++;
                                score += combo * 100;
                                rectanglePrecisionSize = 100;
                                imagePrecision = Image.FromFile("Resources/Images/In Game/100.png");
                                break;
                            }
                            else if (rectangleHitBox4.IntersectsWith(gambar[i].listRectangleBeatMap[3]) && rectangleHitLine1.IntersectsWith(gambar[i].listRectangleBeatMap[3]))
                            {
                                gambar[i].listRectangleBeatMap[3] = Rectangle.Empty;
                                fontSizeCombo = 70;
                                combo++;
                                score += combo * 50;
                                rectanglePrecisionSize = 100;
                                imagePrecision = Image.FromFile("Resources/Images/In Game/50.png");
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    public class highscore : IComparable<highscore>
    {
        public int score { get; set; }
        public string nama { get; set; }

        public highscore(int score, string nama)
        {
            this.score = score;
            this.nama = nama;
        }

        public int CompareTo(highscore other)
        {
            if (this.score == other.score)
            {
                return this.nama.CompareTo(other.nama);
            }
            return other.score.CompareTo(this.score);
        }

        public override string ToString()
        {
            return this.score.ToString() + "-" + this.nama;
        }
    }
}
