﻿namespace Uso
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.p1_A = new System.Windows.Forms.Label();
            this.p1_S = new System.Windows.Forms.Label();
            this.p1_J = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.p2_J = new System.Windows.Forms.Label();
            this.p2_S = new System.Windows.Forms.Label();
            this.p2_A = new System.Windows.Forms.Label();
            this.p1_Ready = new System.Windows.Forms.Label();
            this.p2_Ready = new System.Windows.Forms.Label();
            this.countDownTimer = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // p1_A
            // 
            this.p1_A.BackColor = System.Drawing.Color.DimGray;
            this.p1_A.Font = new System.Drawing.Font("Lucida Console", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p1_A.ForeColor = System.Drawing.Color.White;
            this.p1_A.Location = new System.Drawing.Point(12, 53);
            this.p1_A.Margin = new System.Windows.Forms.Padding(3);
            this.p1_A.Name = "p1_A";
            this.p1_A.Size = new System.Drawing.Size(50, 50);
            this.p1_A.TabIndex = 0;
            this.p1_A.Text = "A";
            this.p1_A.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p1_S
            // 
            this.p1_S.BackColor = System.Drawing.Color.DimGray;
            this.p1_S.Font = new System.Drawing.Font("Lucida Console", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p1_S.ForeColor = System.Drawing.Color.White;
            this.p1_S.Location = new System.Drawing.Point(68, 53);
            this.p1_S.Margin = new System.Windows.Forms.Padding(3);
            this.p1_S.Name = "p1_S";
            this.p1_S.Size = new System.Drawing.Size(50, 50);
            this.p1_S.TabIndex = 0;
            this.p1_S.Text = "S";
            this.p1_S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p1_J
            // 
            this.p1_J.BackColor = System.Drawing.Color.DimGray;
            this.p1_J.Font = new System.Drawing.Font("Lucida Console", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p1_J.ForeColor = System.Drawing.Color.White;
            this.p1_J.Location = new System.Drawing.Point(124, 53);
            this.p1_J.Margin = new System.Windows.Forms.Padding(3);
            this.p1_J.Name = "p1_J";
            this.p1_J.Size = new System.Drawing.Size(50, 50);
            this.p1_J.TabIndex = 0;
            this.p1_J.Text = "J";
            this.p1_J.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.label4.Location = new System.Drawing.Point(12, -4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 54);
            this.label4.TabIndex = 1;
            this.label4.Text = "Player 1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.label1.Location = new System.Drawing.Point(295, -4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 54);
            this.label1.TabIndex = 5;
            this.label1.Text = "Player 2";
            // 
            // p2_J
            // 
            this.p2_J.BackColor = System.Drawing.Color.DimGray;
            this.p2_J.Font = new System.Drawing.Font("Lucida Console", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p2_J.ForeColor = System.Drawing.Color.White;
            this.p2_J.Location = new System.Drawing.Point(407, 53);
            this.p2_J.Margin = new System.Windows.Forms.Padding(3);
            this.p2_J.Name = "p2_J";
            this.p2_J.Size = new System.Drawing.Size(50, 50);
            this.p2_J.TabIndex = 2;
            this.p2_J.Text = "J";
            this.p2_J.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p2_S
            // 
            this.p2_S.BackColor = System.Drawing.Color.DimGray;
            this.p2_S.Font = new System.Drawing.Font("Lucida Console", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p2_S.ForeColor = System.Drawing.Color.White;
            this.p2_S.Location = new System.Drawing.Point(351, 53);
            this.p2_S.Margin = new System.Windows.Forms.Padding(3);
            this.p2_S.Name = "p2_S";
            this.p2_S.Size = new System.Drawing.Size(50, 50);
            this.p2_S.TabIndex = 3;
            this.p2_S.Text = "S";
            this.p2_S.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p2_A
            // 
            this.p2_A.BackColor = System.Drawing.Color.DimGray;
            this.p2_A.Font = new System.Drawing.Font("Lucida Console", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p2_A.ForeColor = System.Drawing.Color.White;
            this.p2_A.Location = new System.Drawing.Point(295, 53);
            this.p2_A.Margin = new System.Windows.Forms.Padding(3);
            this.p2_A.Name = "p2_A";
            this.p2_A.Size = new System.Drawing.Size(50, 50);
            this.p2_A.TabIndex = 4;
            this.p2_A.Text = "A";
            this.p2_A.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p1_Ready
            // 
            this.p1_Ready.BackColor = System.Drawing.Color.DimGray;
            this.p1_Ready.Font = new System.Drawing.Font("Lucida Console", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p1_Ready.ForeColor = System.Drawing.Color.White;
            this.p1_Ready.Location = new System.Drawing.Point(31, 109);
            this.p1_Ready.Margin = new System.Windows.Forms.Padding(3);
            this.p1_Ready.Name = "p1_Ready";
            this.p1_Ready.Size = new System.Drawing.Size(162, 50);
            this.p1_Ready.TabIndex = 0;
            this.p1_Ready.Text = "ENTER";
            this.p1_Ready.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // p2_Ready
            // 
            this.p2_Ready.BackColor = System.Drawing.Color.DimGray;
            this.p2_Ready.Font = new System.Drawing.Font("Lucida Console", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p2_Ready.ForeColor = System.Drawing.Color.White;
            this.p2_Ready.Location = new System.Drawing.Point(319, 109);
            this.p2_Ready.Margin = new System.Windows.Forms.Padding(3);
            this.p2_Ready.Name = "p2_Ready";
            this.p2_Ready.Size = new System.Drawing.Size(162, 50);
            this.p2_Ready.TabIndex = 0;
            this.p2_Ready.Text = "ENTER";
            this.p2_Ready.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // countDownTimer
            // 
            this.countDownTimer.Interval = 1000;
            this.countDownTimer.Tick += new System.EventHandler(this.countDownTimer_Tick);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.DimGray;
            this.label2.Font = new System.Drawing.Font("Lucida Console", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(180, 53);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 50);
            this.label2.TabIndex = 6;
            this.label2.Text = "K";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.DimGray;
            this.label3.Font = new System.Drawing.Font("Lucida Console", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(463, 53);
            this.label3.Margin = new System.Windows.Forms.Padding(3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 50);
            this.label3.TabIndex = 7;
            this.label3.Text = "K";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 28F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(529, 192);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.p2_J);
            this.Controls.Add(this.p2_S);
            this.Controls.Add(this.p2_A);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.p1_J);
            this.Controls.Add(this.p1_S);
            this.Controls.Add(this.p2_Ready);
            this.Controls.Add(this.p1_Ready);
            this.Controls.Add(this.p1_A);
            this.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Verifikasi! | Tekan ENTER untuk READY.";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VerifyControl_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label p1_A;
        private System.Windows.Forms.Label p1_S;
        private System.Windows.Forms.Label p1_J;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label p2_J;
        private System.Windows.Forms.Label p2_S;
        private System.Windows.Forms.Label p2_A;
        private System.Windows.Forms.Label p1_Ready;
        private System.Windows.Forms.Label p2_Ready;
        private System.Windows.Forms.Timer countDownTimer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}