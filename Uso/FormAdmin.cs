﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Uso
{
    public partial class FormAdmin : Form
    {
        DataGridView dg;
        public Form1 parent;
        int datatype;
        public FormAdmin(int type)
        {
            InitializeComponent();
            datatype = type;
        }

        private void FormListUser_Load(object sender, EventArgs e)
        {
            this.Width = 600;
            this.Height = 400;
            dg = new DataGridView();
            dg.AllowUserToDeleteRows = false;
            dg.AllowUserToAddRows = false;
            dg.EditMode = DataGridViewEditMode.EditProgrammatically;
            dg.Size = this.Size;
            this.Controls.Add(dg);
            //jikaform di load dengan parameter 0 maka tampilkan data users jika 1 maka tampilkan data lagu
            if (datatype == 0)
            {
                //users
                this.Text = "List User";
                dg.Rows.Clear();
                dg.Columns.Clear();
                dg.Columns.Add("No", "No");
                dg.Columns.Add("Username", "Username");
                dg.Columns.Add("Password", "Password");
                for (int i = 0; i < parent.listUser.Count; i++)
                {
                    dg.Rows.Add(i,parent.listUser[i].username,parent.listUser[i].password);
                }
                //MessageBox.Show("Users");
            }
            else if(datatype == 1)
            {
                //lagu
                this.Text = "List Lagu";
                dg.Rows.Clear();
                dg.Columns.Clear();
                dg.Columns.Add("No","No");
                dg.Columns.Add("SongName","Song Name");
                dg.Columns.Add("SongPath","Song Path");
                for (int i = 0; i < parent.songPaths.Count; i++)
                {
                    string songname = parent.songPaths[i].Remove(0, parent.songPaths[i].LastIndexOf("/") + 1);
                    dg.Rows.Add(i,songname,parent.songPaths[i]);
                }
                //MessageBox.Show("Lagu");
            }
        }
    }
}
