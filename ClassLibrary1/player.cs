﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace ClassLibrary1
{
    [Serializable]
    public class player
    {
        public string username { get; set; }
        public string password { get; set; }

        public player(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
    }
}
