﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClassLibrary1
{
    [Serializable]
    public class option
    {
        public Keys[] keybind { get; set; }
        public int volume { get; set; }

        public option()
        {
            keybind = new Keys[4];
            volume = 50;
            keybind[0] = Keys.A;
            keybind[1] = Keys.S;
            keybind[2] = Keys.J;
            keybind[3] = Keys.K;
        }
    }
}
