﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace ClassLibrary1
{
    [Serializable]
    public class user
    {
        public string username { get; set; }
        public string password { get; set; }
        public option customOption { get; set; }

        public user(string username, string password)
        {
            this.username = username;
            this.password = password;
            customOption = new option();
        }
    }
}
