﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ClassLibrary1
{
    public class beatmap
    {
        public List<Rectangle> listRectangleBeatMap = new List<Rectangle>();
        public double waktuMuncul { get; set; }

        public beatmap(int x0, int x1, int x2, int x3, double waktuMuncul)
        {
            listRectangleBeatMap.Add(new Rectangle(x0, 0, 60, 15));
            listRectangleBeatMap.Add(new Rectangle(x1, 0, 60, 15));
            listRectangleBeatMap.Add(new Rectangle(x2, 0, 60, 15));
            listRectangleBeatMap.Add(new Rectangle(x3, 0, 60, 15));
            this.waktuMuncul = waktuMuncul;
        }

        public void gerak(ref int combo, ref Image imagePrecision, ref int rectanglePrecisionSize)
        {
            for (int i = 0; i < listRectangleBeatMap.Count; i++)
            {
                listRectangleBeatMap[i] = new Rectangle(listRectangleBeatMap[i].X, listRectangleBeatMap[i].Y + 5, 60, 15);
                if (listRectangleBeatMap[i].Y > 565)
                {
                    if (listRectangleBeatMap[i].X != 0)
                    {
                        combo = 0;
                        imagePrecision = Image.FromFile("Resources/Images/In Game/miss.png");
                        rectanglePrecisionSize = 100;
                    }
                    listRectangleBeatMap[i] = Rectangle.Empty;
                }
            }
        }
    }
}
